#!/bin/bash

echo "Installing additional dependent packages"
sudo apt-get update

# Install basic software
sudo apt-get -y install sudo wget git-core ca-certificates

# add postgresql repository
# suggest that we have 	squeeze
#sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ squeeze-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# automatically detect the debian codename of the release
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release 2>/dev/null -a | grep Codename: | awk \"{print \\$2}\"`-pdg main" > /etc/apt/sources.list.d/pgdg.list' 
sudo apt-get install -y wget ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update



sudo apt-get -y install build-essential libxml2-dev libgeos-dev libpq-dev libbz2-dev libtool automake libproj-dev
sudo apt-get -y install libboost-dev libboost-system-dev libboost-filesystem-dev libboost-thread-dev libexpat-dev

sudo apt-get -y install gcc proj-bin libgeos-c1 libgeos++-dev
sudo apt-get -y install postgresql-9.4 postgis-2.1 postgresql-contrib-9.4 postgresql-9.4-postgis-2.1 postgresql-server-dev-9.4
sudo apt-get -y install libprotobuf-c0-dev protobuf-c-compiler

sudo aptitude -y install build-essential python-dev protobuf-compiler \
                      libprotobuf-dev libtokyocabinet-dev python-psycopg2 \
                      libgeos-c1

sudo apt-get -y install python-gdal
sudo aptitude -y install libgdal1-dev libspatialindex-dev
sudo apt-get install -y python-setuptools
sudo easy_install pip
sudo pip install --upgrade pip virtualenv virtualenvwrapper
sudo apt-get install -y  osmctools

# needed for  postgres-XC and postgres-XL compilation
sudo apt-get install -y flex
sudo apt-get install -y bison
sudo apt-get install -y jade 
sudo apt-get install -y docbook
sudo apt-get install -y docbook-dsssl
sudo apt-get install -y docbook-xsl
sudo apt-get install -y libreadline-dev

# Install imposm2
sudo pip install rtree
sudo pip install imposm


# Install imposm.geocoder
#wget https://bitbucket.org/quiqua/imposm.geocoder/get/27f3f45b4353.zip
#unzip 27f3f45b4353.zip
#cd quiqua-imposm.geocoder-27f3f45b4353
cd imposm.geocoder
sudo python setup.py install 
cd -
#rm -r quiqua-imposm.geocoder-27f3f45b4353 27f3f45b4353.zip

# Install JSON-RPC WSGI Framework
sudo pip install Flask==0.10.1 dill==0.2.2 Werkzeug==0.10.4 jsonrpclib==0.1.3 json-rpc==1.9.1 Jinja2==2.7.3
sudo pip install line_profiler

# Build Levenshtein Distance Calculator
cd lib/Levenshtein
python setup.py build
find ./build -iname "_levenshtein.so" -exec cp {} . \;
rm -r build
cd -

#sudo apt-get install zlib1g-dev libncurses5-dev libreadline6-dev  libexpat1-dev libdb4.8-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev -y >/dev/null
#sudo apt-get install  liblapack-dev libblas-dev -y >/dev/null
#sudo apt-get install libfreetype6-dev libpng-dev -y >/dev/null
#sudo apt-get install subversion -y >/dev/null
