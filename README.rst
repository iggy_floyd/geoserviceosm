================================================================================================================================
OSM Geolocator as a Postal Address Validation Tool 
================================================================================================================================






:Author: Igor Marfin
:Contact: igor.marfin@unister.de
:Organization: private
:Date: Nov 6, 2015, 9:28:51 AM
:Status: draft
:Version: 1
:Copyright: This document has been placed in the public domain. You
            may do with it as you wish. You may copy, modify,
            redistribute, reattribute, sell, buy, rent, lease,
            destroy, or improve it, quote it at length, excerpt,
            incorporate, collate, fold, staple, or mutilate it, or do
            anything else to it that your or anyone else's heart
            desires.


|
|


.. admonition:: Dedication

    For Unister users & co-developers.



|
|


.. admonition:: Abstract

    This  project is the Geolocation service utilizing the Open Street Maps and addopted to validate the postal addresses wide-world.



|
|    

.. meta::
   :keywords: Open Steet Maps, Geocoding, local installation
   :description lang=en: OSM Geolocator as a Postal Address Validation Tool 



|
|




.. contents:: Table of Contents





----------------------------------------------------------------
Introduction to the topic and motivation
----------------------------------------------------------------


I want to introduce you to my motivation which forces me to develop this project.

One of the biggest problem in the  management of the customer relations is validation of the 
addresses supplied by customers. One can naively expect that addresses might be not valid because of:

* misspellings and mistakes made by customers during the filling online forms of web pages
* bugs in the front-end (Javascript-based) filters used to validate the input from users
* bugs in the back-end logic of portals: related to a PHP MVC framework or to database queries etc
* fake addresses given by a person due-to the reason of  protection of his/her identity

The naive questions are raised: 
 
* Can we use OSM to validate addresses?
* Can we use OSM for address verification in commerical product?

Yes, we can. The reason of this 'can' is explained below.


Open Street Maps [#OSM]_  allow ::

    free (or almost free) access to our map images and all of our underlying map data. 
    The project aims to promote new and interesting uses of this data. See "Why OpenStreetMap?" 
    For more details about why we want an open-content map and for the answer to the question we hear most frequently: Why not just use Google maps? 



From [#OSM_MAP_FEATURES]_, the OSM maps provide not only geodata but also 'tags' to geolocations: ::

   OpenStreetMap represents physical features on the ground (e.g., roads or buildings) using tags attached to its basic data structures 
   (its nodes, ways, and relations). Each tag describes a geographic attribute of the feature being shown by that specific node, way or relation.

   OpenStreetMap's free tagging system allows the map to include an unlimited number of attributes describing each feature. 
   The community agrees on certain key and value combinations for the most commonly used tags, which act as informal standards. 
   However, users can create new tags to improve the style of the map or to support analyses that rely on previously unmapped attributes of the features. 
   Short descriptions of tags that relate to particular topics or interests can be found using the feature pages. 


Such tags contain postal codes, building numbers, poi and other interesting information. Quality and number of the tags is instantly growing which improves the 
"presentation" of the  postal addresses in OSM as well. OSM are being update each minute. All these factors, undoubtly, force me to consider the OSM as 
a nice tool to be used for "preliminary" address verifications.



What is the behind this service?
=====================================================



The Backend of the service is PostgreSQL with PostGis extender [#PostGis]_.

The importing data is done with the help of the ``Imposm2`` [#Imposm2]_. The ``Imposm2``
is an importer for OpenStreetMap data. It reads XML and PBF files and can import the data into PostgreSQL/PostGIS databases. 
It is designed to create databases that are optimized for rendering/WMS services.
It is developed and supported by Omniscale, runs on Linux or Mac OS X and is released as open source under the Apache Software License 2.0.


I have applied the ``imposm.geocoder`` api [#ImposmGeocoder]_  with small modifications to perform the geocoding of the 
addresses requested for validation. 


The ``Werkzeug`` [#Werkzeug]_ and ``jsonrpc-lib`` [#JSONRPCLIB]_ were used to implement the ``pythonic`` json-rpc server to supply
the Rest http-service.




----------------------------------------------------------------
Installation of the package
----------------------------------------------------------------


.. Important::
  
    
    The project relies on the presence of the Vagrant and VirtualBox tools in the system
    Before, proceed further, please, install them.

Simply, clone the project 

.. code-block:: bash

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/geoserviceosm.git


and wake-``up`` the vagrant VM:


.. code-block:: 

    cd GeoServiceOSM
    vagrant up    


It starts the installing all needed libraries in the VM. 



Deployment
======================================


When the provisioning the VM has been finished, one can deploy the sever

.. code-block:: 
    
    vagrant ssh
    cd ~/project
    source ./deploy.sh


The script ``deploy.sh`` reproduces some operational steps described in the file ``HOW-TO``:


.. code-block:: 
    
    
    # prepare configuration of the service: use predefined values from template
    cp template/config.ini.template configs/config.ini 
    
    # set the locale for the postgis and postgres: en_US.UTF-8
    sudo scripts/set_locale.sh

    # set the .pgpass file for osm user in the PostgreSQL DB
    source scripts/create-db-password.sh

    # prepare the script for initialization of the PostgreSQL DB
    cp templates/create-db.sh.template scripts/create-db.sh

    # set up the DB
    sudo -u postgres scripts/create-db.sh
    sudo /etc/init.d/postgresql restart

    # apply patches to fix small bugs in the osmimport and osmimport.geocoder apis
    sudo cp hotfix/app.py.imposm.geocoder /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/app.py
    sudo cp hotfix/tables.py /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/tables.py
    sudo cp hotfix/__init__.py  /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/__init__.py
    sudo cp hotfix/app.py.imposm /usr/local/lib/python2.7/dist-packages/imposm/app.py

    # download osm maps for regions defined in the configs/config.ini
    scripts/download_maps.sh

    # import these maps to the DB
    scripts/import_maps.sh 

    # add a cron job for daily updating the OSM maps 
    scripts/create-cronjob-update.sh
 

    # start daemons: address validation and updating services
    cd daemons
    ./setup.daemons.sh
    ./process_all_daemons.sh start
    cd -


That's it.


Postgres-XL Backend
======================================


Postgres-XL is a horizontally scalable open source SQL database cluster, based on the PostgreSQL, 
which is flexible enough to handle varying database workloads [#Postgreslx]_.

Experiments, made by me, has shown that the geolocation server on 'one-process' postgresql provides the 
perfomance about 900-1200 ms per request. The significant reduction of this query time is needed.
Several technologies, like ``'pg_shard'``, ``'postgres-XC'`` and ``'postgres-xl'`` have been tested in order to improve
performance. More details can be found in the correspoding folders

::

    postgresql_sharding/
    postgres-xc/
    postgres-xl/



Only ``Postgres-xl`` was able to show the acceptible results. To deploy the server with the Postgres-xl [#Postgreslx]_ backend, you can do


.. code-block::

    vagrant ssh
    cd ~/project
    export POSTGRESXL_BACKEND=yes; source ./deploy.sh


.. Important::

   The postgres-XL server should be installed before ``export POSTGRESXL_BACKEND=yes; source ./deploy.sh``. In order to install it, please,address yourself to 
   the tutorial: ``cat postgres-xl/HOW-TO-XL``.



Tests
======================


The folder ``test/`` contains the python script ``test_osm_geocoder_service.py`` which can be used to test a few addresses from Bremen (Bremen and Hamburg OSM are imported
during the deployment),Germany region. Simply start the daemon of ``bin/osm_geocoder_service.py`` 
(it is being started during the demployment, if it is not the case, see the subsection below), and run the commands


.. code-block:: 

   
   cd test
   ./test_osm_geocoder_service.py



Daemons
=====================



In order to start the python server and the shell script used to update OSM maps as daemons without using /etc/init.d and other dedicated stuff,
I have added the simple framework, which allows to run any executable as a daemon. This includes
the files

* ``daemons/setup.daemon.sh``
* ``daemons/daemon.sh``
* ``daemons/_oobash.sh``
* ``daemons/respawn_double_fork.sh``

The ``daemons/setup.daemon.sh`` defines properties  of the daemons:

* DAEMON NAME -- a unique string to identify daemon settings stored in the ``bin/config.ini``
* DAEMON UUID -- a unique 8-letters string to identify daemon process in the system. This
  uuid is used to check and kill the process in the system
* DAEMON CMD -- a command line used by the daemon to start the executable 

The  oo-style framework  ``OOBash``  is used to handle these properties as attributes of the object
``DAEMON`` in OOP style.

The ``daemons/daemon.sh`` is a control script to start/stop/check_status of any daemon. It starts internally 
the script ``daemons/respawn_double_fork.sh`` as a double-forked process which is respawned, i.e.
``( ${RESPAWNSCRIPT} \"${DAEMON_CMD}\" 1 \"\\--${DAEMON_UUID}\" \"${KILLWORD}\" & ) &``.
What doest it mean: ``DOUBLE-FORKED and RESPAWNED``? ``DOUBLE-FORKED`` means that it becomes the child of the init process. 
Because it is ``RESPAWNED``, i.e. it has an internal loop, it never dies!
``daemons/respawn_double_fork.sh`` uses  ``netcat`` to listen the port 9760 on the localhost.
The only one way to stop ``${RESPAWNSCRIPT}`` is to  send the message  via netcat:

.. code-block:: 

    echo -ne ${KILLWORD} | nc -q1  localhost 9760


That is what ``bin/daemon.sh stop`` makes.

So, in order, to start, for example, ``bin/osm_geocoder_service.py`` as a daemon, one can do

.. code-block:: 
    
   cd daemons;
   ./setup.daemons.sh;
   export DAEMON_NAME=GEOSERVICE; 
   ./daemon.sh start;
    
  
      

----------------------------------------------------------------
Documentation
----------------------------------------------------------------

The main parts of documentation are placed  in the ``README.rst``.

The python and shell codes in ``bin/`` and ``lib/`` folders are documented as well.
The docstring are used for this purpose.

The ``Pylit`` frameworks and  two scripts: ``source_doc/create_documentation.sh`` and
``source_doc/shell2rst.py`` can be used to extract the docstrings and create 
the documentation of the source code as ``source_doc/{bin,lib}/*.rst`` files.


Running the command,

.. code-block:: 
    
    make doc
    
    
one will make a folder ``doc/`` with the ``README.html`` which collects all ``*.rst`` files together in
the HTML format.



----------------------------------------------------------------
Samples of addresses
----------------------------------------------------------------



Because of the privacy protection reasons, the samples are excluded from the release.



----------------------------------------------------------------
References
----------------------------------------------------------------


.. [#OSM] http://wiki.openstreetmap.org/wiki/Main_Page
.. [#OSM_MAP_FEATURES] http://wiki.openstreetmap.org/wiki/Map_Features
.. [#PostGis] http://postgis.net/
.. [#Imposm2] http://imposm.org/
.. [#ImposmGeocoder] https://bitbucket.org/quiqua/imposm.geocoder/
.. [#Werkzeug] http://werkzeug.pocoo.org/
.. [#JSONRPCLIB] https://github.com/joshmarshall/jsonrpclib/
.. [#Postgreslx] http://www.postgres-xl.org/overview/


.. target-notes::
    


-----------------------------------------
 Documentation of the source code 
-----------------------------------------

Here will be the documentation of the code from 
``source_doc/{bin,lib,daemons,scripts}/*rst`` inserted.

.. include:: source_doc/bin/osm_geocoder_service.py.rst
.. include:: source_doc/lib/Logger.py.rst
.. include:: source_doc/lib/settings.py.rst
.. include:: source_doc/lib/BestAddress.py.rst
.. include:: source_doc/daemons/respawn_double_fork.sh.rst
.. include:: source_doc/daemons/setup.daemons.sh.rst
.. include:: source_doc/daemons/process_all_daemons.sh.rst
.. include:: source_doc/daemons/daemon.sh.rst    
.. include:: source_doc/daemons/daemon_update.sh.rst    
.. include:: source_doc/daemons/daemon_geoservice.sh.rst    
.. include:: source_doc/daemons/daemon_fake.sh.rst    
