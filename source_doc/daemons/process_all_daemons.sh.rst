   
   


Daemon control script for any Fake program 
============================================================== 
 
:Date: Apr 23, 2015, 12:59:09 PM 
:File:   process_all_daemons.sh 
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de    


This script controls all daemons in the system 
    


Usage 
------------------------------- 
 
./process_all_daemons.sh <start|stop|status>    
   
   


The code of the script 
----------------------- 
 
::    
   




   message() {
   
   
    
   
       cat << _END > _tmp_file_$$
   
   		Hi!
   
   	You can try the following options:
   		* $1 start  -- to start all daemons
   		* $1 stop   -- to stop all daemons
   		* $1 restart -- to restart all daemons
   
           You can abort all daemons, by the command:
   		ps aux | grep bktree | awk '{print \$2}' | xargs -I {} kill -9 {}
   		
   _END
          cat _tmp_file_$$
          rm -f _tmp_file_$$;
   }
   
   




   do_stop() {
   
       for key in ${keys[@]}
       do
           local service=${key,,};
           [[ "${service}" == "fake" ]] && continue;
           if [ -z "${DAEMON_BACKUP_ROTATION_NOTRESTART}" -a "$service" == "update" ]
           then
               ./daemon_${service}.sh stop
           elif [ "$service" != "update" ]
           then
               ./daemon_${service}.sh stop
           fi
       done
       return 0;
   }
   




   do_start(){
   
       for key in ${keys[@]}
       do
           local service=${key,,};
           [[ "${service}" == "fake" ]] && continue;
           if [ -z "${DAEMON_BACKUP_ROTATION_NOTRESTART}" -a "$service" == "update" ]
           then
               ./daemon_${service}.sh start
           elif [ "$service" != "update" ]
           then
               ./daemon_${service}.sh start
           fi
       done
       return 0;
   
   }
   




   do_status(){
       for key in ${keys[@]}
       do
           local service=${key,,};
           [[ "${service}" == "fake" ]] && continue;
           ./daemon_${service}.sh status
       done
       return 0;
   }
   




   main() {
   
   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   _parent_dir="${_local_dir}/.."
   
   




   _setuppath="${_local_dir}"
   _daemons_cfg="${_setuppath}/daemons.cfg";
   if [ -f "${_daemons_cfg}" ]
   then
       source ${_daemons_cfg};
   fi
   
   keys=("${!daemon_settings[@]}");
   [[  ${#keys} -eq 0 ]] && { echo "daemon settings -- not found!!"; exit 1; }
   
   
   case "$2" in
    
   	start|stop|status)
   		do_${2}
   	;;
   
   	restart|reload|force-reload)
   		do_stop
   		do_start
   	;;
    
   	*)
   	echo;
   	echo "Usage: $1 {start|stop|restart}";
   	echo;
   	message;	
   	exit 1;
   	;;
    
   esac
   exit 0
   
   }	
   
   
   
   main $0 $1;
