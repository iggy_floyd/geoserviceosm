   


Daemon control script for any Fake program 
============================================================== 
 
:Date: Apr 23, 2015, 12:59:09 PM 
:File:   daemon_fake.sh 
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de    


This script controls the 'fake' daemon in the system. 
    


Usage 
------------------------------- 
 
./daemon_fake.sh <start|stop|status>    
   
   


The code of the script 
----------------------- 
 
::    
   




   export DAEMON_NAME=FAKE
   ./daemon.sh $@