   


Daemon control script 
============================== 
 
:Date: Apr 23, 2015, 12:59:09 PM 
:File:   daemon.sh 
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de    


This script controls the daemon in the system. 
    


Usage 
------------------------------- 
 
./daemon.sh <start|stop|status>     
   


OOBash -  oo-style framework for bash 4 
----------------------------------------------------- 
 
::    




   source _oobash.sh 
   


Make sure to delete all objects at end of program 



   trap 'delete_all' TERM EXIT INT
   
   


The code of the script 
----------------------- 
 
::    




   _setuppath=`pwd`;
   




   _daemons_cfg="${_setuppath}/daemons.cfg";
   if [ -f "${_daemons_cfg}" ]
   then
       source ${_daemons_cfg}
   fi
   
   keys=("${!daemon_settings[@]}")
   
   
   
   [ -z "${DAEMON_NAME}" ] && DAEMON_NAME=FAKE
   




   if [  ${#keys} -eq 0 ]
   then
       RANDOM_SUFFIX=`cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1`;
       CMD="${_setuppath}/fake.sh --${RANDOM_SUFFIX}";
       PORT_IN=9760
       PORT_OUT=9761
       PORT_PROG_IN=9762
       STOPWORD="FAKE_STOP"
   else




       new DAEMON daemonobj "$DAEMON_NAME" "<>" 0 1 2 3 "<>";
       RANDOM_SUFFIX=$($daemonobj.restore_uuid daemon_settings);
       PORT_IN=$($daemonobj.restore_indaemonport daemon_settings);
       PORT_OUT=$($daemonobj.restore_outdaemonport daemon_settings);
       PORT_PROG_IN=$($daemonobj.restore_inprogport daemon_settings);
       PORT_PROG_OUT=$($daemonobj.restore_outprogport daemon_settings);
       STOPWORD=$($daemonobj.restore_stopword daemon_settings);
       CMD="$($daemonobj.restore_cmd daemon_settings) --${RANDOM_SUFFIX}"
   fi
   
   




   RESPAWNSCRIPT=${_setuppath}/respawn_double_fork.sh;
   




   KILLWORD="${RANDOM_SUFFIX}";
   




   DAEMON="( ${RESPAWNSCRIPT} \"${CMD}\" 1 \"\\--${RANDOM_SUFFIX}\" \"${KILLWORD}\" ${PORT_IN} ${PORT_OUT} & ) &";
   _basename=`echo "$CMD" | awk '{print $1;}'`;
   




   DAEMON_PATTERN1=`basename "$_basename"`;
   DAEMON_PATTERN2="\"\\--${RANDOM_SUFFIX}\""
   




   . /lib/lsb/init-functions
   
   
   
   do_start () {
   
   
   	log_daemon_msg "Starting $DAEMON_PATTERN1"
   
   	if [ -z "$DAEMON" ] 
   	then 
   		 log_end_msg 1; 
   		exit 1;
   	fi
   
   	echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null
   	if [ $? -eq 0 ]
   	then
   		log_end_msg 1;
   		return 1;
   	else
   		 echo $DAEMON | bash;
   		 local _status=$?
   		 log_end_msg $_status
   	fi
   
   }
   
   
   
   do_stop () {
   	log_daemon_msg "Stopping $DAEMON_PATTERN1"
   
   
   	[ -n "$DAEMON" ] &&  
   	[  `which nc` ] && 
   	echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null &&
   	echo  "$KILLWORD" | nc -q1  localhost ${PORT_IN} 1>/dev/null 2>/dev/null &&
   	{
          local _status=$?;
          log_end_msg $_status;
          echo  "$STOPWORD" | nc -q1  localhost ${PORT_PROG_IN} 1>/dev/null 2>/dev/null;
          echo -n;
       } 	||  log_end_msg 1
   
   }
   
   
   case "$1" in
    
   start|stop)
   	do_${1}
   	;;
    
   restart|reload|force-reload)
   	do_stop
   	do_start
   	;;
    
   status)
    	log_daemon_msg  "Status of $DAEMON_PATTERN1"
   	[ -n "$DAEMON" ]  && echo "ps aux | grep $DAEMON_PATTERN1 | grep $DAEMON_PATTERN2 | grep -v grep" | bash >/dev/null &&
       {
        _status=$?;
        log_end_msg $_status;
       } ||  log_end_msg 1
   
   ;;
   *)
   echo "Usage: $0 {start|stop|restart|status}"
   exit 1
   ;;
    
   esac
   exit 0
