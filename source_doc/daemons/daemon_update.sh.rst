   


Daemon control script for the process updating OSM 
============================================================== 
 
:Date: Apr 23, 2015, 12:59:09 PM 
:File:   daemon_update.sh 
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de    


This script controls the 'update' daemon in the system. 
    


Usage 
------------------------------- 
 
./daemon_update.sh <start|stop|status>    
   
   


The code of the script 
----------------------- 
 
::    
   




   export DAEMON_NAME=UPDATE
   ./daemon.sh $@
