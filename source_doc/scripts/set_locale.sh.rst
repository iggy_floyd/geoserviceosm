

Script to install Nominatim on Ubuntu 
Tested on 14.04 (View Ubuntu version using 'lsb_release -a') using Postgres 9.3 
 
Based on OSM Nominatim wiki: 
http://wiki.openstreetmap.org/wiki/Nominatim/Installation 
Synced with: Latest revision as of 21:43, 21 May 2015    


!! Marker #idempotent indicates limit of testing for idempotency - it has not yet been possible to make it fully idempotent.    




   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   


reading ini file for further initialization 



   source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)
   
   
   


Ensure the system locale is UTF-8, to avoid Postgres install failure 



   echo "LANG=${utf8_language}.UTF-8" > /etc/default/locale
   echo "LC_ALL=${utf8_language}.UTF-8" >> /etc/default/locale
   sudo locale-gen ${utf8_language} ${utf8_language}.UTF-8
   dpkg-reconfigure locales
   
