

Script to install Nominatim on Ubuntu 
Tested on 14.04 (View Ubuntu version using 'lsb_release -a') using Postgres 9.3 
    




   set -xe
   
   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   _parent_dir="${_local_dir}/.."
   _script_dir="${_local_dir}/../scripts"
   _DATETIMEFORMAT="+%Y_%m_%d_%H_%M_%S"
   
   


reading ini file for further initialization 



   source <(
   	grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
   )
   


Logging service initialization 



   LOG_TAG=BACKUP_SYSTEM;
   export LOG_FILE="${_parent_dir}"/"${geoservice_logging_relative_path}";
   
   if  [ -z  $BL_LEVEL  ]
   then
   
           cd "${_script_dir}"
           . bashlog;
           cd - 2>&1 /dev/null;
   fi
   
   
   


directory where we want to store backup 



   backup_dir="${_parent_dir}/${database_backupdb_relative_path}"
   
   


create a backup folder 



   mkdir -p "${backup_dir}" 2>/dev/null
   
   cd "${backup_dir}"
   _file_output="${database_backupdb_prefix}"_`date "${_DATETIMEFORMAT}"`
   pg_dump -i -h "${imposm_postgis_host}" -p "${imposm_postgis_port}" -U "${database_user}"  -F c -b -v -f  "${_file_output}"  "${database_name}"
   _bashlog INFO "Backup of the Postgresql has been made: ${_file_output}";
   cd - 2>&1 /dev/null;
   
   
   exit 0
   set +x
