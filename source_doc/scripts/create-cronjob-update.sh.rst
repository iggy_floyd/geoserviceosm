

Script to install Nominatim on Ubuntu 
Tested on 14.04 (View Ubuntu version using 'lsb_release -a') using Postgres 9.3 
    


et -xe    




   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   _parent_dir="${_local_dir}/.."
   _script_dir="${_local_dir}/../scripts"
   _daemon_dir="${_local_dir}/../daemons"
   


reading ini file for further initialization 



   source <(
   	grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
   )
   


Logging service initialization 



   LOG_TAG=CRON_JOB_CREATOR;
   export LOG_FILE="${_parent_dir}"/"${geoservice_logging_relative_path}";
   
   if  [ -z  $BL_LEVEL  ]
   then
   
           cd "${_script_dir}"
           . bashlog;
           cd - 2>&1 /dev/null;
   fi
   


daemon support 



   source ${_daemon_dir}/_oobash.sh
   




   _daemons_cfg="${_daemon_dir}/daemons.cfg";
   if [ -f "${_daemons_cfg}" ]
   then
       source ${_daemons_cfg}
   fi
   
   keys=("${!daemon_settings[@]}")
   
   if [  ${#keys} -eq 0 ]
   then
   
       _port_in=9880
       _port_out=9881
   else
        new DAEMON daemonobj "UPDATE" "<>" 0 1 2 3 "<>";
       _port_in=$($daemonobj.restore_inprogport daemon_settings);
       _port_out=$($daemonobj.restore_outprogport daemon_settings);
   fi
   
   
   _cron_job="echo -ne 'UPDATE_UPDATE' | nc -q1  localhost   ${_port_in}"


cho "${_cron_job}" 



   (crontab -l ; echo "00 00 * * 6 ${_cron_job}") | crontab -
   
   echo "You can delete the job via the call: 
   
   	VISUAL=nano; crontab -e
   "
   exit 0


et +x 