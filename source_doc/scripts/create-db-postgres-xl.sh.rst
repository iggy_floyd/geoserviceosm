   




   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   _parent_dir="${_local_dir}/.."
   


reading ini file for further initialization 



   source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)
   
   


et -xe    
   




   cd "${_parent_dir}"/postgres-xl
   
   [[ ! -d  _build2  ]] && echo "Please, install the Postgres-XL first. Tutorial is given in HOW-TO-XL" && exit 1
   
   
   
   


1)  Creation of the cluster of several datanodes and one coordinator 



   datanodes=(`ls postgresql.conf.datanode* | sed -e 's/postgresql.conf.datanode//'`)
   for datanode in "${datanodes[@]}"
    do
     echo "Initialization of the Datanode-$datanode"
     sudo mkdir "/var/lib/postgresql/data_datanode${datanode}"
     sudo chown postgres.postgres "/var/lib/postgresql/data_datanode${datanode}"
     sudo -u postgres _build2/bin/initdb -D "/var/lib/postgresql/data_datanode${datanode}"  --nodename "datanode${datanode}"
    done
   




   sudo mkdir /var/lib/postgresql/data_coord1
   sudo mkdir /var/lib/postgresql/data_gtm
   sudo chown postgres.postgres /var/lib/postgresql/data_coord1
   sudo chown postgres.postgres /var/lib/postgresql/data_gtm
   sudo -u postgres _build2/bin/initdb -D /var/lib/postgresql/data_coord1  --nodename coord1
   sudo -u postgres _build2/bin/initgtm -D /var/lib/postgresql/data_gtm -Z gtm
   
   


2) Update  configurations 



   sudo -u postgres cp postgresql.conf.coordinator  /var/lib/postgresql/data_coord1/postgresql.conf
   for datanode in "${datanodes[@]}"
    do
      sudo -u postgres cp "postgresql.conf.datanode${datanode}"  "/var/lib/postgresql/data_datanode${datanode}/postgresql.conf"
    done
   


an  associative array to store parameters of the cluster 
cluster_params[datanode_name]="datanode_type datanode_port" 



   declare -A cluster_params 
   
   cluster_params[coord1]="coordinator 5432" # add coordinator's info
   


3) START of the cluster 



   sudo -u postgres _build2/bin/gtm  -D /var/lib/postgresql/data_gtm >logfile 2>&1 &
   sudo -u postgres _build2/bin/postgres --coordinator -D /var/lib/postgresql/data_coord1  >>logfile 2>&1 &
   
   for datanode in "${datanodes[@]}"
    do
     _port=`cat postgresql.conf.datanode${datanode} | grep port | grep change | sed -e 's/\(.*\)#.*/\1/' | sed -e 's/.*=\(.*\)/\1/'`
     _port=`echo $_port | tr -d ' '` 
     sudo -u postgres _build2/bin/postgres --datanode -p $_port -D  /var/lib/postgresql/data_datanode${datanode} >>logfile 2>&1 &
     cluster_params[datanode${datanode}]="datanode $_port"
    done
   


4) Add Connections from  Coordinator to the DataNodes 



   for datanode in "${datanodes[@]}"
    do
      _port=`cat postgresql.conf.datanode${datanode} | grep port | grep change | sed -e 's/\(.*\)#.*/\1/' | sed -e 's/.*=\(.*\)/\1/'`
      _port=`echo $_port | tr -d ' '` 
      sleep 2
      echo "CREATE NODE datanode${datanode} WITH (TYPE = 'datanode', PORT = ${_port})" | sudo -u postgres _build2/bin/psql


  sudo -u postgres _build2/bin/psql -c "CREATE NODE datanode${datanode} WITH (TYPE = 'datanode', PORT = ${_port})" postgres 



      sleep 2
    done
   


TO-DO: introduce loops to alter all datanode automatically! 
4.1)  A BUG-FIX of the 'INSERT PROBLEM'. The below loops reproduce the recipe: 



   echo "
   
   A BUG-FIX of the 'INSERT PROBLEM':
   
   	Postgres-XL has a problem with INSERT() queries.
           We have two datanodes with (names,ports) as (datanode1, 15432) and (datanode2, 15433).
           So, to fix the problem,  we need to update 'communication' tables via the code below
   
          #  datanode1
          echo \"CREATE NODE datanode2 WITH (TYPE = 'datanode', PORT = 15433)\" | sudo -u postgres _build2/bin/psql -p 15432
          echo \"CREATE NODE coord1 WITH (TYPE = 'coordinator', PORT = 5432 )\" | sudo -u postgres _build2/bin/psql -p 15432
          echo \"ALTER NODE datanode1 WITH (TYPE = 'datanode', PORT = 15432)\" | sudo -u postgres _build2/bin/psql -p 15432
   	
          # datanode2
          echo \"CREATE NODE datanode1 WITH (TYPE = 'datanode', PORT = 15432)\"| sudo -u postgres _build2/bin/psql -p 15433
          echo \"CREATE NODE coord1 WITH (TYPE = 'coordinator', PORT = 5432 )\"| sudo -u postgres _build2/bin/psql -p 15433
          echo \"ALTER NODE datanode2 WITH (TYPE = 'datanode', PORT = 15433)\"| sudo -u postgres _build2/bin/psql -p 15433
   
       
          # reload informative tables
          echo \"SELECT pgxc_pool_reload()\" | sudo -u postgres _build2/bin/psql -p 15432
          echo \"SELECT pgxc_pool_reload()\" | sudo -u postgres _build2/bin/psql -p 15433
          echo \"SELECT pgxc_pool_reload()\" | sudo -u postgres _build2/bin/psql
   
   "
   




   for node in "${!cluster_params[@]}"
    do
     _node_info=(${cluster_params[$node]})
     _type_node="${_node_info[0]}"
     _port_node="${_node_info[1]}"
      [[ "$_type_node" == "datanode" ]] && echo "ALTER NODE $node WITH (TYPE = 'datanode', PORT = $_port_node)"  | sudo -u postgres _build2/bin/psql -p $_port_node
      [[ "$_type_node" == "coordinator" ]] && continue
      for node2 in "${!cluster_params[@]}"
       do       
          [[ "$node" == "$node2" ]] && continue
          _node_info2=(${cluster_params[$node2]})
          _type_node2="${_node_info2[0]}"
          _port_node2="${_node_info2[1]}"
          echo "CREATE NODE $node2 WITH (TYPE = '$_type_node2', PORT = $_port_node2)" | sudo -u postgres _build2/bin/psql -p $_port_node
       done
    done
   




   for node in "${!cluster_params[@]}"
    do
     _node_info=(${cluster_params[$node]})
     _type_node="${_node_info[0]}"
     _port_node="${_node_info[1]}"
     echo "SELECT pgxc_pool_reload()" | sudo -u postgres _build2/bin/psql -p $_port_node
    done
   




   for node in "${!cluster_params[@]}"
    do
     _node_info=(${cluster_params[$node]})
     _type_node="${_node_info[0]}"
     _port_node="${_node_info[1]}"
   
     echo "
   
   	$node: 
   
     "
     echo "SELECT * FROM pgxc_node" | sudo -u postgres _build2/bin/psql -p $_port_node
    done
   
   
   


Create a user and a database and enable the extenstions 



   echo "Create a user and a database and enable the extenstions"
   sudo -u postgres _build2/bin/createuser --no-superuser --no-createrole --createdb "${database_user}"
   sudo -u postgres _build2/bin/createdb  --locale=${utf8_language}.UTF-8 -E UTF-8  -O "${database_user}"  "${database_name}"
   sudo -u postgres createlang plpgsql "${database_name}" ||:
   sudo -u postgres _build2/bin/psql -f  _build2/share/contrib/postgis-2.1/postgis.sql  -d "${database_name}"
   sudo -u postgres _build2/bin/psql -f  _build2/share/contrib/postgis-2.1/spatial_ref_sys.sql  -d "${database_name}"
   sudo -u postgres _build2/bin/psql -f  /usr/local/lib/python2.7/dist-packages/imposm/900913.sql -d  "${database_name}"
   
   echo "ALTER TABLE geometry_columns OWNER TO ${database_user};" | sudo -u postgres _build2/bin/psql -d "${database_name}"
   echo "ALTER TABLE spatial_ref_sys OWNER TO ${database_user};" | sudo -u postgres _build2/bin/psql -d "${database_name}"
   echo "ALTER USER ${database_user} WITH PASSWORD '${database_passwd}';" | sudo -u postgres _build2/bin/psql -d "${database_name}"
   echo "CREATE EXTENSION pg_trgm;"  | sudo -u postgres _build2/bin/psql -d "${database_name}"
   
   
   
   


et +x 



   echo "Done. Don't forget to restart postgresql! via"
   echo "${_local_dir}/restart-db-postgres-xl.sh"
