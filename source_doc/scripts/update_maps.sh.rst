

Script to install Nominatim on Ubuntu 
Tested on 14.04 (View Ubuntu version using 'lsb_release -a') using Postgres 9.3 
    




   set -xe
   
   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   _parent_dir="${_local_dir}/.."
   _script_dir="${_local_dir}/../scripts"


reading ini file for further initialization 



   source <(
   	grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
   )
   
   


directory where all pbf files are stored 



   data_dir="${_parent_dir}/${localdata_relative_path}"
   
   


cd to the data dir 



   cd  ${data_dir}
   
   
   
   places=(${place_osm})
   for place in "${places[@]}"
    do
    echo "We are processing the place: $place"
    region_osm=$(dirname "${place}")
    _place_osm=$(basename "${place}")
    _file_pbf="${region_osm}/${_place_osm}-latest.osm.pbf"
    _file_pbf_md5="${_file_pbf}.md5"
   
    cd "${region_osm}/"
     # do updating
     osmupdate "${_place_osm}-latest.osm.pbf"  "new_${_place_osm}-latest.osm.pbf" -v
     mv "new_${_place_osm}-latest.osm.pbf" "${_place_osm}-latest.osm.pbf"
    cd -
   
    done
   


do importing 



   ${_script_dir}/import_maps.sh
   
   cd ${_script_dir}
   
   exit 0
   set +x
