

Script to install Nominatim on Ubuntu 
Tested on 14.04 (View Ubuntu version using 'lsb_release -a') using Postgres 9.3 
    




   set -xe
   
   _abs_path_to_this_file=$(readlink -f "$0")
   _local_dir=$(dirname "$_abs_path_to_this_file")
   _config_dir="${_local_dir}/../configs"
   _parent_dir="${_local_dir}/.."
   


reading ini file for further initialization 



   source <(
   	grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
   )
   
   
   data_dir="${_parent_dir}/${localdata_relative_path}"
   
   places=(${place_osm})
   for place in "${places[@]}"
    do
    echo "We are processing the place: $place"
    region_osm=$(dirname "${place}")
    _place_osm=$(basename "${place}")
    _file_pbf="${region_osm}/${_place_osm}-latest.osm.pbf"
    _file_pbf_md5="${_file_pbf}.md5"
   
    # create a data dir
    mkdir -p "${data_dir}/${region_osm}" 2>/dev/null
   
    # Download OSM data if not already present
   
    [[  -f  "${data_dir}/${_file_pbf}" ]] && [[ "${update_osm}" = "1" ]] && rm  "${data_dir}/${_file_pbf}"
    [[  -f  "${data_dir}/${_file_pbf_md5}" ]] &&  [[ "${update_osm}" = "1" ]] && rm  "${data_dir}/${_file_pbf_md5}"
   
    _continue=
    [[  -f  "${data_dir}/${_file_pbf}" ]] && _continue=" -c "
    wget ${_continue} --output-document="${data_dir}/${_file_pbf}"  "${url_osm}/${_file_pbf}"
   
    _continue=
    [[  -f  "${data_dir}/${_file_pbf_md5}" ]] && _continue=" -c "
    wget ${_continue} --output-document="${data_dir}/${_file_pbf_md5}"  "${url_osm}/${_file_pbf_md5}"
   
    [[ "$(md5sum ${data_dir}/${_file_pbf} | awk '{print $1;}')" != "$(cat ${data_dir}/${_file_pbf_md5} | awk '{print $1;}')" ]] && echo "Something wrong with MD5 CheckSum!!" && exit 1
    [[ "$(md5sum ${data_dir}/${_file_pbf} | awk '{print $1;}')" == "$(cat ${data_dir}/${_file_pbf_md5} | awk '{print $1;}')" ]]  && echo "MD5 CheckSum is OK!" 
   
    done
   
   exit 0
   set +x
