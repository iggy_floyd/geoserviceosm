..  #! /usr/bin/env python
  # -*- coding: utf-8 -*-
  
OSM Geocoder Service
==================================================================

:Date: Apr 23, 2015, 12:59:09 PM
:File:   osm_geocoder_service.py
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de



::

  '''
      OSM Geocoder Service
  '''
  
  
  __author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
  __date__ ="$Apr 23, 2015 12:59:09 PM$"
  __docformat__ = 'restructuredtext'
  __version__ = "0.0.1"
  
  
  
Requirements
------------

::


  import sys
  import traceback
  import sys
  import time
  import multiprocessing as mp
  import copy
  import json
   
  
  from werkzeug.wrappers import Request, Response
  from werkzeug.serving import run_simple
  from jsonrpc import JSONRPCResponseManager, dispatcher
  from os import sys, path
  from SocketServer import ThreadingMixIn
  sys.path.append(path.dirname(path.dirname(path.abspath(__file__)))+'/lib/')
  from Logger import mylogging
  from SimilarityCalculator import *
  
  sys.path.append('/usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/')
  #from geocoder.model import init_model, geocode, close_model,profile
  from geocoder.model import init_model, geocode, close_model
  from geocoder.config import load_config
  import geocoder.app
  from subprocess import Popen,PIPE
  
  
  
  from settings import geocoding_server_port,mylogging_threshold,config_dir_path,geocoding_configs
  mylogging.thresholdVerbosity= mylogging_threshold
  
  
  # used to gather profiling information
  #from line_profiler import LineProfiler
  #from kernprof import ContextualProfile
  
  
`find_address(street,housenumber,city,country="",similarity_limit=0.70)`
------------------------------------------------------------------------------
find an address in the OSM maps


=============== ============================= ================================================================================================
:param:         street (string)               a street of the address to be validated
:param:         housenumber (string)          a housenumber of he address to be validated
:param:         city (string)                 a city of the address to be validated
:param:         country (string)              a country of the address to be validated
:param:         similarity_limit (float)      a minimal value of the Levenshtein distance in the PostgresSQL pg_trgm search index
:returns:       {'status':1,'result':{}.,\    if an exception was thrown
                'error':str(e)}
:returns:       {'status':0,'result':res}     if no exception was thrown
:example:       a json request                {"jsonrpc":"2.0","id": "1","method":"check_ip","params":[""]}
=============== ============================= ================================================================================================

::

  #profile = ContextualProfile()
  #profile = LineProfiler()
  
  shema_configs=[load_config(config_dir_path+schema_config[1]) for schema_config in geocoding_configs]
  #similarity_limit=0.92
  #levenshtein_limit=min(1.,1.-max(0.,similarity_limit))
  #for i,config in enumerate(shema_configs):
  # if (i>0):
  #  init_model(config,levenshtein_limit=levenshtein_limit,non_primary=True)
  # else:
  #  init_model(config,levenshtein_limit=levenshtein_limit)
  
  
  counter=0
  initialized=False
  
  @dispatcher.add_method
  #@profile
  def find_address(street,housenumber,zipcode,city,country="",similarity_limit=0.70):
      '''find an address in the OSM maps'''
      global config_dir_path,geocoding_configs,counter,shema_configs,initialized
      try:        
          res={'addresses':[]}
          levenshtein_limit=min(1.,1.-max(0.,similarity_limit))
          for i,geocoding_config in enumerate(geocoding_configs):
  #         print geocoding_config[0],'!!!!!!!!!!!!!!!!'
  #         if (i==0): continue
  
  ##         config = load_config(config_dir_path+geocoding_config[1])        
           config = shema_configs[i]        
  #         if (counter>0):
  #          init_model(config,levenshtein_limit=levenshtein_limit,non_primary=True)
  #         else:
  #          print 'config 1 initialization'
  #          init_model(config,levenshtein_limit=levenshtein_limit)
  #         counter+=1
  
           if not(initialized):
  #           print "initialization!!!!"
             config = shema_configs[i]        
             init_model(config,levenshtein_limit=levenshtein_limit)
             initialized=True
           params = {
              'city': translate(city),
              'road': translate(street),
              'housenumber': translate(housenumber),
              'postcode': translate(zipcode),
              'country': country
  #            'city': city,
  #            'road': street,
  #            'housenumber': housenumber,
  #            'postcode': zipcode,
  #            'country': country
           }
           try:
  #          print "00000000000000"
  #          print params                    
            result = geocode(params)
  #          print "result=",result
           
           # here we extract information
            list_result_processed=[]
            if (len(result)==0): continue
            result_processed={}
            for _res in result:
             try:
  #            print "\n\n", _res,"\n\n"
              if not("properties" in _res): continue
  #            print "1"
              if not("query_type" in _res['properties']): continue
  #            print "2"
              if not("address" in _res['properties']): continue
  #            print "3"
              if  _res['properties']['query_type'] != 'OSMQueryAddress': continue
  #           print "4"
              if not("geometry" in _res): continue
  #            print "5"
              if not("coordinates" in _res['geometry']): continue
  #            print "6"
              if len( _res['geometry']['coordinates'])<1: continue
  #            print "7"
  
  #            print "_res['properties']['address']['city']=",_res['properties']['address']['city']
              city_found =  _res['properties']['address']['city']
              country_found =  _res['properties']['address']['country']
              street_found =  _res['properties']['address']['street']
              zipcode_found =  _res['properties']['address']['postcode']
              housenumber_found = _res['properties']['address']['housenumber']
              name = _res['properties']['address']['name']
              street;housenumber;zipcode;city
              addr_in=';'.join([params['road'],params['housenumber'],params['postcode'],params['city']])
              addr_out=';'.join([street_found,housenumber_found,zipcode_found,city_found])
              addr_to_display = "%s %s,%s,%s"%(street_found,housenumber_found,zipcode_found,city_found)
              similarity_calc=SimilarityCalculator()
              total_similarity=similarity_calc(addr_in,addr_out)
              similarities,avg_sqrt_similarity,min_similarity = similarity_calc.address_similarity(addr_in,addr_out)
              (lat,lng) =  _res['geometry']['coordinates'][0][1],_res['geometry']['coordinates'][0][0]
  
              result_processed={
                'coordinates':(lat,lng),
                'total_similarity':total_similarity,
                'min_similarity':min_similarity,
                'avg_sqrt_similarity':avg_sqrt_similarity,
                'similarity_street':similarities[0],
                'similarity_housenumber':similarities[1],
                'similarity_zipcode':similarities[2],
                'similarity_city':similarities[3],
                'addr_to_display':addr_to_display,
                'name':name,
                'country':country_found
              }
             except Exception,e2:
              print "E2=",e2
              result_processed={}
              continue  
             list_result_processed+=[result_processed]
           except Exception,e1:
  #          print "E1=",e1
            mylogging.setVerbosity('ERROR')(str(e1))       
  #          result=[]
            list_result_processed=[]
            close_model()
  #          counter=0
            initialized=False
           #res['addresses']+=[{geocoding_config[0]:result}]
           res['addresses']+=[{geocoding_config[0]:list_result_processed}]
      except Exception,e:
              mylogging.setVerbosity('ERROR')("Something wrong with address search has been happened ... ")
              mylogging.setVerbosity('ERROR')(str(e))
              close_model()
  #            counter=0
              initialized=False 
              return {'status':1,'result':{'addresses':[]},'error':str(e)} # 0 if no errors, 1 otherwise
  #    close_model()
  #    counter=0
  #    initialized=False
      return {'status':0,'result':res} # 0 if no errors, 1 otherwise
  
  
  
`find_address_v2(street,housenumber,city,country="")`
------------------------------------------------------------------------------
find an address in the OSM maps. Geocoding is done with running the linux command: 'imposm-geocoder geocode --config config -c Bremen....etc'


=============== ============================= ================================================================================================
:param:         street (string)               a street of the address to be validated
:param:         housenumber (string)          a housenumber of he address to be validated
:param:         city (string)                 a city of the address to be validated
:param:         country (string)              a country of the address to be validated
:returns:       {'status':1,'result':{}.,\    if an exception was thrown
                'error':str(e)}
:returns:       {'status':0,'result':res}     if no exception was thrown
:example:       a json request                {"jsonrpc":"2.0","id": "1","method":"check_ip","params":[""]}
=============== ============================= ================================================================================================

::

  @dispatcher.add_method
  #@profile
  def find_address_v2(street,housenumber,zipcode,city,country=""):
      '''find an address in the OSM maps'''
      global config_dir_path,geocoding_configs
      try:        
          res={'addresses':[]}
          for i,geocoding_config in enumerate(geocoding_configs):
           config=config_dir_path+geocoding_config[1]
           params = {
              'city': translate(city),
              'road': translate(street),
              'housenumber': translate(housenumber),
              'postcode': translate(zipcode),
              'country': country }
           try:
             cmd_args=['geocode', '--config', config, '-c','"'+ params['city'] + '"','-r', '"'+params['road']+'"','-n','"'+params['housenumber']+'"','-p','"'+params['postcode']+'"']
             if len(params['country'])>0: cmd_args+=['-x','"'+params['country']+'"']
             print 'imposm-geocoder ',' '.join(cmd_args)
             #result=json.load(geocoder.app.main(cmd_args))
             (out,err) = Popen(['imposm-geocoder']+cmd_args, stdout=PIPE).communicate()
             result = out
           except Exception,e1:
            mylogging.setVerbosity('ERROR')(str(e1))       
            result=[]
           res['addresses']+=[{geocoding_config[0]:result}]
      except Exception,e:
              mylogging.setVerbosity('ERROR')("Something wrong with address search has been happened ... ")
              mylogging.setVerbosity('ERROR')(str(e))
              return {'status':1,'result':{'addresses':[]},'error':str(e)} # 0 if no errors, 1 otherwise
      return {'status':0,'result':res} # 0 if no errors, 1 otherwise
  
  
  
  
The SIGINT and SIGTERM hanlder
--------------------------------------------
The handler ``close_service()`` properly when SIGINT or SIGTERM are sent to the application.

::


  import signal
  def sigterm_handler(signum, frame):
  #    global profile
      mylogging.setVerbosity('INFO')('OSM Geocoder Service ...stopped')
  #    mylogging.setVerbosity('INFO')('OSM Geocoder Service ... dumping profiling info')
  #    profile.dump_stats('osm_geocoder_service.py.lprof')
      sys.exit(0)
  def sigint_handler(signum, frame):
  #    global profile
      mylogging.setVerbosity('INFO')('OSM Geocoder Service ...stopped')
  #    mylogging.setVerbosity('INFO')('OSM Geocoder Service ... dumping profiling info')
  #    profile.dump_stats('osm_geocoder_service.py.lprof')
      sys.exit(0)
  signal.signal(signal.SIGTERM, sigterm_handler)
  signal.signal(signal.SIGINT, sigint_handler)
  
  
The Werkzeug http application definition
--------------------------------------------
::

  @Request.application
  def application(request):
      response = JSONRPCResponseManager.handle(
          request.data, dispatcher)
      return Response(response.json, mimetype='application/json')
  
  
The verbosity level of the server
-----------------------------------
::

  import logging
  from settings import log_level
  logging.getLogger('werkzeug').setLevel(log_level)
  
  
  
The main entrance point of the proxy_detector_service.py
----------------------------------------------------------------

 .. code-block:: bash

                 vagrant@FPS-Fraud-Classifier2:~/project/bin> ./proxy_detector_service.py

::

  if __name__ == '__main__':
      try:
          mylogging.setVerbosity('INFO')('Staring OSM Geocoder Service ... ')
          run_simple('', geocoding_server_port, application)
      except:
          mylogging.setVerbosity('INFO')('OSM Geocoder Service  ...stopped')
          pass
