..  # -*- coding: utf-8 -*-
  '''
          Stores the settings of the different classes
  '''
  
 File:   settings.py
 Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
 Created on Apr 23, 2015, 12:59:09 PM


::

  __author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
  __date__ ="$Apr 23, 2015 12:59:09 PM$"
  
  
  from os import sys, path
  import logging
  
  log_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/log/"
  config_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/configs/"
  lib_dir_path=path.dirname(path.dirname(path.abspath(__file__)))+"/lib/"
  unknown_keyword='('+'_'*5 + 'UNKNOWN' + '_'*5 + ')'
  
  # gecoder configs
  #geocoding_configs=[('schema_1','config_client_1.ini'),('schema_2','config_client_2.ini')]
  geocoding_configs=[("schema_1","config_client_1.ini.new")]
  
  
  # a geocoder rest api client 
  geocoding_rest_server_port= 8092
  geocoding_rest_host="http://192.168.56.116:%d"%geocoding_rest_server_port
  
  # a geocoder client 
  geocoding_server_port= 8095 
  geocoding_host="http://192.168.56.116:%d"%geocoding_server_port 
  
  # verbosity level
  # suppress logging messages from werkzeug server
  log_level= logging.ERROR # to see all possible levels levels, see  https://docs.python.org/2/library/logging.html#levels
  #log_level= logging.NOTSET # to see all possible levels levels, see  https://docs.python.org/2/library/logging.html#levels
  mylogging_threshold=7 # debug level by default
  
