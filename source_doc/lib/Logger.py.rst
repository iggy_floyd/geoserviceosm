..  #! /usr/bin/env python
  # -*- coding: utf-8 -*-
  
Logging service
==============================

:Date: Apr 23, 2015, 12:59:09 PM
:File:   Logger.py
:Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de



::

  '''
     The Logging Service
  
  '''
  
  __author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
  __date__ ="$Apr 23, 2015 12:59:09 PM$"
  __docformat__ = 'restructuredtext'
  __version__ = "0.0.1"
  
  
Requirements
------------

::

  import logging
  import sys
  import inspect
  from settings import log_dir_path
  
  
Logger
------------
The class Logger presents the logging service of important events which can be happened during training, classification, backuping etc.
::

  class Logger(object):
      ''' Logger to log events in FPS detector '''
      
      
      userEventID=115
      
                              
      def __init__(self,LOG_FILENAME = log_dir_path+'history.log',
              format='+'*100 + '\n'+'|%(asctime)s|%(levelname)s|%(message)s \n' +'-'*100,
              datefmt='%d.%m.%Y %H:%M:%S',
              level=logging.DEBUG
              ):
          self.LOG_FILENAME = LOG_FILENAME
          self.format=format
          self.datefmt=datefmt
          #self.level = logging.DEBUG
          self.level = Logger.userEventID
          self.handlers={}
          
          logging.basicConfig(filename= self.LOG_FILENAME,
                              filemode='a',
                              format=self.format,
                              datefmt=self.datefmt,
                              level=115)
  
      def addEventHandler(self,EventName='Event',id=None,storeHandler=True):
          ''' add new handler of logging Events of type EventName'''
  
          if (storeHandler): 
              self.handlers[EventName.lower()]=(EventName,id)
          setattr(logging,EventName.upper(),Logger.userEventID if ((id is None) or (type(id) != int))  else id)
  
          logging.addLevelName(getattr(logging,EventName.upper()), EventName.upper())
          
          setattr(
                      logging.Logger,
                      EventName.lower(),  
                      lambda inst, msg, *args, **kwargs: inst.log(getattr(logging,EventName.upper()), msg, *args, **kwargs)
          
                  )
          setattr(
                  logging,
                  EventName.lower(),
                  lambda msg, *args, **kwargs: logging.log(getattr(logging,EventName.upper()), msg, *args, **kwargs)
                  )
                  
          
          Logger.userEventID+=1 if ((id is None) or (type(id) != int)) else 0
          return self
          
          
      def write2Log(self,type='BACKUP',msg='Some action was done'):
          ''' make the writting to the log file about some action,
              i.e backuping or updating    '''
          
          
          if not(hasattr(logging,type.lower())) and (type.lower() in self.handlers):
              self.addEventHandler(EventName=self.handlers[type.lower()][0],id=self.handlers[type.lower()][1],storeHandler=False)
          
          
          # writting to log file
          getattr(logging,type.lower())(msg)
          #getattr(logging.Logger,type.lower())(msg)
          
          return self
      
      
      def cleanLog(self):        
          ''' clean the content of the log file '''
          
          open(self.LOG_FILENAME,'w').close()
      
  
  
  
  
LogOut
------------
The class redirect the logging information to stdout.
::

  class LogOut(object):
      ''' the class outputs the logging messages to stdout. '''
      def __init__(self, filename="logs",category='Unknown'):
          self.terminal = sys.stdout
          self.category = category
          self.log = open(filename, "a")
  
      def write(self, message):
          if not message: return
          if (len(message)==0): return
          self.terminal.write(self.category+': '+message+'\n')
          self.log.write(self.category+': '+message+'\n')
          
  
mylogging
------------
The function is used everywhere in the code of the FPS classifier to printout debug messages.
All messages are flushed to stdout and the log file `log_dir_path+'logs'`.
::

  def mylogging(*kargs):
      '''
  
      The function serves to print messages at the different logging levels.
  
      Example of use
      (fraud-classifier)vagrant@FPS-Fraud-Classifier2:~/project/lib$ python
  
      >>> from Logger import mylogging
      >>> mylogging('test')
          [Thu Sep 10 17:22:31 2015]  [*file*: <stdin> *function*: <module>  *line*: 1]  [DEBUG]  [  test  ]
      >>> mylogging.setVerbosity('ERROR')('test')
          [Thu Sep 10 17:22:35 2015]  [*file*: <stdin> *function*: <module>  *line*: 1]  [ERROR]  [  test  ]
      >>> mylogging.setVerbosityThreshold('ERROR').setVerbosity('ERROR')('test')
          [Thu Sep 10 17:22:57 2015]  [*file*: <stdin> *function*: <module>  *line*: 1]  [ERROR]  [  test  ]
      >>> mylogging.setVerbosityThreshold('ERROR').setVerbosity('DEBUG')('test')
      >>> mylogging.setVerbosityThreshold('INFO').setVerbosity('ERROR')('test')
          [Thu Sep 10 17:24:01 2015]  [*file*: <stdin> *function*: <module>  *line*: 1]  [ERROR]  [  test  ]
      '''
      import time
      verbosities = {
          7:'DEBUG',
          6:'INFO',
          5:'NOTICE',
          4:'WARN',
          3:'ERROR',
          2:'CRIT',
          1:'ALERT',
          0:'EMERGENGY'
      }
      inv_verbosities = {
          'DEBUG':7,
          'INFO':6,
          'NOTICE':5,
          'WARN':4,
          'ERROR':3,
          'CRIT':2,
          'ALERT':1,
          'EMERGENGY':0
      }
      mylogging.locals = locals().copy()
      _atts = dir(mylogging)
      if 'levelVerbosity' not in  _atts:
       mylogging.levelVerbosity = 7
      if 'thresholdVerbosity' not in  _atts:
       mylogging.thresholdVerbosity = 7
      if 'print2stdout' not in   _atts:
       mylogging.print2stdout = True
      if 'resetVerbosity' not in   _atts:
       mylogging.resetVerbosity = True
  
  
      def setVerbosity(x):
          ''' changes the verbosity'''
          mylogging.levelVerbosity = mylogging.locals['inv_verbosities'][x]
          return mylogging
  
      def setVerbosityThreshold(x):
          ''' changes the threshold of the  verbosity'''
          mylogging.thresholdVerbosity = mylogging.locals['inv_verbosities'][x]
          return mylogging
  
      mylogging.setVerbosity = lambda x: setVerbosity(x)
      mylogging.setVerbosityThreshold = lambda x: setVerbosityThreshold(x)
  
  
  
      if (mylogging.levelVerbosity>mylogging.thresholdVerbosity): return
      if (len(kargs)==0): return
      _stack = "[*file*: %s *function*: %s  *line*: %s] "%(inspect.stack()[1][1],inspect.stack()[1][3],inspect.stack()[1][2])
      _time = '[%s] '%(str(time.strftime("%c")))
      _verbosity='[%s] '%verbosities[mylogging.levelVerbosity]
      kargs = map(lambda x: str(x),kargs)
      if (mylogging.print2stdout): print _time,_stack,_verbosity,'[ ',' '.join(kargs),' ]'
      print >>open(log_dir_path+'logs','a'),_time,_stack,_verbosity,'[ ',' '.join(kargs),' ]'
  
      # reset the levelVerbosity to the highest value: 'DEBUG'
      if (mylogging.resetVerbosity): mylogging.levelVerbosity = 7
  
  
mylogging
~~~~~~~~~~~~
``mylogging`` requires the initialization
::


  mylogging() # initialization of the 'mylogging' service
  
  
The test suite of the Logger.py
-------------------------------------

 .. code-block:: bash

                 vagrant@FPS-Fraud-Classifier2:~/project/lib> python Logger.py

::


  if __name__ == "__main__":
      
      
      
      # Test
      logger = Logger()
      logger.addEventHandler('Update_Database')
      logger.addEventHandler('Backup_Database')
      logger.addEventHandler('Transform_JSON')
      logger.addEventHandler('Transform_Database')
      logger.addEventHandler('Transform_Variable')
      
      logger.write2Log('Update_Database','Database is updated')
      logger.write2Log('Transform_Database','Database is transformed')
      logger.write2Log('Transform_Variable','Database is transformed using binary transformation')
      
      
      print open(logger.LOG_FILENAME).readlines()
      logger.cleanLog()
      print open(logger.LOG_FILENAME).readlines()
      
