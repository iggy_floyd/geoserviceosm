CREATE INDEX idx_places_name_trgm ON osm_niedersachsen_places USING gist (name gist_trgm_ops);
CREATE INDEX idx_places_lookup_name_trgm ON osm_niedersachsen_places USING gist (lookup_name gist_trgm_ops);
CREATE INDEX idx_roads_merged_name_trgm ON osm_niedersachsen_roads_merged USING gist (name gist_trgm_ops);
CREATE INDEX idx_point_address_name_trgm ON osm_niedersachsen_point_addresses USING gist (name gist_trgm_ops);
CREATE INDEX idx_point_address_city_trgm ON osm_niedersachsen_point_addresses USING gist ("addr:city" gist_trgm_ops);
CREATE INDEX idx_point_address_street_trgm ON osm_niedersachsen_point_addresses USING gist ("addr:street" gist_trgm_ops);
CREATE INDEX idx_point_address_type_trgm ON osm_niedersachsen_point_addresses USING gist (type gist_trgm_ops);
CREATE INDEX idx_point_address_country_trgm ON osm_niedersachsen_point_addresses USING gist ("addr:country" gist_trgm_ops);
CREATE INDEX idx_polygon_address_name_trgm ON osm_niedersachsen_polygon_addresses USING gist (name gist_trgm_ops);
CREATE INDEX idx_polygon_address_city_trgm ON osm_niedersachsen_polygon_addresses USING gist ("addr:city" gist_trgm_ops);
CREATE INDEX idx_polygon_address_street_trgm ON osm_niedersachsen_polygon_addresses USING gist ("addr:street" gist_trgm_ops);
CREATE INDEX idx_polygon_address_type_trgm ON osm_niedersachsen_polygon_addresses USING gist (type gist_trgm_ops);
CREATE INDEX idx_polygon_address_country_trgm ON osm_niedersachsen_point_addresses USING gist ("addr:country" gist_trgm_ops);

-- TODO add trgm for housenumbers, a possible value is 14-17 for any value, plus add an index.
-- TODO check if gist is really slower than gin, first test proved the opposite and thus the documentation may be false for pg_trgm! :(
    
-- DROP INDEX idx_places_name_trgm;
-- DROP INDEX idx_unified_roads_name_trgm;
-- DROP INDEX idx_point_address_type_trgm;
-- DROP INDEX idx_point_address_name_trgm;
-- DROP INDEX idx_point_address_city_trgm;
-- DROP INDEX idx_point_address_street_trgm;
-- DROP INDEX idx_point_address_country_trgm;
-- DROP INDEX idx_polygon_address_type_trgm;
-- DROP INDEX idx_polygon_address_name_trgm;
-- DROP INDEX idx_polygon_address_city_trgm;
-- DROP INDEX idx_polygon_address_street_trgm;
-- DROP INDEX idx_polygon_address_country_trgm;
