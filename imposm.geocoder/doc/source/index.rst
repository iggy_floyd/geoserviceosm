Imposm.geocoder
===============

Imposm.geocoder is a geocoder for OpenStreetMap data. It is developed as a libary and can be used to convert addresses into coordinates.

Right now the geocoder is still in an early beta state and some code is optimized for German addresses.
Be aware that the API will change in the future.

It depends on an PostgreSQL/PostGIS database and the import tool `Imposm <http://imposm.org/>`_.
It is developed and supported by `Omniscale <http://omniscale.com>`_.
The libary runs on Linux or Mac OS X and is released as open source under the `Apache License 2.0 <http://www.apache.org/licenses/LICENSE-2.0.html>`_.

Features
--------

Geocode addresses
  You can calculate a coordinate for an address, city, street, postcode and country or a combination of these.

Fuzzy string matching
  The geocoder performs a fuzzy string match to search for data in your database. This tolerates spelling errors.
  
Lookup names
  Defines a new column type for places and removes location-describing words and suffixes.

Merge line segements
  Merge line segments of the same road to a single line string to avoid repeated reaults.


Development
-----------

The source code is available at: https://bitbucket.org/quiqua/imposm.geocoder/

You can report any issues at: https://bitbucket.org/quiqua/imposm.geocoder/issues

Contents
--------

.. toctree::
   :maxdepth: 2

   install
   import
   geocode
   datamapping


.. Indices and tables
.. ==================
.. 
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

