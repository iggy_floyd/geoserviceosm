Data Mapping
============

The data mapping defines which `OSM feature types <http://wiki.openstreetmap.org/wiki/Map_Features>`_ should be imported in which table. The mapping is described with a Python file using classes from ``imposm.mapping`` package.

The geocoder implements additional classes in the package ``imposm.geocoder.mapping``.
Furthermore a minimal mapping built for geocoding will be shown. These classes and the mapping are described below.

For a detailed information on data mapping, please read the `Imposm data mapping chapter <http://imposm.org/docs/imposm/latest/mapping.html>`_.

Additional Tables
-----------------

Table GeometryUnionTable
~~~~~~~~~~~~~~~~~~~~~~~~

This class creates a new table from a given source table. The objects from the source table are grouped by an certain attribute and their geometries will be unified.

For example::

  postcodes_unified = GeometryUnionTable(
      name = 'postcodes_union',
      origin = postcodes,
      group_by = 'type',
  )

Table GeometryLineMergeTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Since roads often are separated in multiple segments, this class creates a table where the geometries of each road are merged together into a PostGIS MultiLineString geometry.
This class can be used to create a table where each road is only stored once.

For example::

  roads_merged = GeometryLineMergeTable(
      name = 'roads_merged',
      origin = roads,
      group_by = 'name',
  )
  
Additional Column Type
----------------------

Column Type TrigramIndex
~~~~~~~~~~~~~~~~~~~~~~~~

Imposm has a feature to add a trigram index to a column of a table and is usually used for a fuzzy string match.
This column type ``TrigramIndex`` is a mixin class and will be used to index ``String()`` type columns as shown with the column type ``TrigramString``.

Column Type TrigramString
~~~~~~~~~~~~~~~~~~~~~~~~~

In order to index a ``String()`` type column, you can set this column type in your mapping file.
If you want to index the ``name`` column, you have to define an additional entry in the ``fields`` list.

For Example::

  motorways = Highway(
      name = 'motorways',
      fields = Highway.fields + (
          ('name', TrigramString()),
      ),
      mapping = {
          'highway': (
              'motorway',
              'motorway_link',
              'trunk',
              'trunk_link',
          ),
      }
  )

Column Type CityLookupNameGerman
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Each map feature has a name attribute. Places are often tagged with names that contain prepositions and location-describing names.
In Germany a geographical feature such as a river or a mountain may be added to the name. This is mainly used for places with common names like ``Rothenburg``. A location-describing name would be::

    Rothenburg ob der Tauber
    Rothenburg, Münster
    Rothenburg/Oberlausitz
  
The column type CityLookupNameGerman removes the describing words and creates a lookup name for each place.
As the name suggests, this column type only supports German names.
You have to add the column to the corresponding fields in your mapping file.
In addition, this class inherits from ``TrigramString`` to set an index to the corresponding column.

For Example::

  places = Points(
      name = 'places',
      mapping = {
          'place': (
              'country',
              'state',
              'region',
              'county',
              'city',
              'town',
              'village',
              'hamlet',
              'suburb',
              'locality',
          ),
      },
      fields = (
          ('z_order', ZOrder([
              'country',
              'state',
              'region',
              'county',
              'city',
              'town',
              'village',
              'hamlet',
              'suburb',
              'locality',
          ])),
          ('population', Integer()),
          ('lookup_name', CityLookupNameGerman()),
          ('name', TrigramString()),
      ),
  )
  
Required Mapping Entries
------------------------

In order to geocode addresses or just roads or places you need to have the following entries in your mapping file::

  places,
  admin,
  addresses,
  postcodes,
  roads,
  postcodes_unified,
  roads_merged

A mapping file, which fulfills the minimum criteria can be found at `imposm.geocoder repository <https://bitbucket.org/quiqua/imposm.geocoder/src/default/imposm/geocoder/geocodemapping.py>`_.