Import and Preparation
======================

In order to use the geocoder, you must import the OpenStreetMap data with Imposm and a modified import mapping.

There are several steps involved in importing the data and preparing it for geocoding. Please read the `Imposm documentation <http://imposm.org/docs/imposm/latest/>` first, if you are not familiar with the import process. 

Create database
---------------

In order to create the database, please read the `Imposm Tutorial`_.


Enable Trigram Indexing
-----------------------

A trigram index is important for fast fuzzy string matching, which is used by the geocoder.

The PostgreSQL distribution ships several modules which extend the basic functions, datatypes and operations and the ``pg_trgm`` is one of them.

If you created the database as stated in the previous step, then you can enable the trigram module as ``postgres`` user with the following command::

  $ sudo su postgres
  $ psql -d osm -f /path/to/contrib/pg_trgm.sql
  
On Linux with PostgreSQL 9.0 the module should be located at ``/usr/share/postgresql/9.0/contrib/pg_trgm.sql``.

Starting with PostgreSQL version 9.1 you have to enable the trigram module with::

  $ sudo su postgres
  $ psql -d osm -c "CREATE EXTENSION pg_trgm"

Reading and Writing data
------------------------

The next step reads and writes data with Imposm.
It is required to use the mapping file supplied by the geocoder to make things work like intended::

  imposm --read hamburg.osm.bz2 --write -d osm -U osm -h 127.0.0.1 -m geocoder.defaultmapping.py 

For a detailed step-by-step information for all options, please read the `Imposm tutorial`_.

The mapping file required for geocoding can be found and downloaded at the `imposm.geocoder repository <https://bitbucket.org/quiqua/imposm.geocoder/src/default/imposm/geocoder/defaultmapping.py>`_.


Prepare the data
----------------

The last step is to prepare the data and create two additional tables, which are defined in the mapping file.
All roads and postcodes will be processed and optimized for the geocoding task. New PostGIS functions are created as well as the tables::

  imposm-geocoder prepare -d <database> -U <username> -h <host> -m geocoder.defaultmapping.py


.. _`Imposm tutorial`: http://imposm.org/docs/imposm/latest/tutorial.html
