Usage
=====

Config
______

The geocoder itself uses a config file, where the database connection as well as the neccessary tablenames are defined::

  [database]
  dialect = postgresql
  user = osm
  password = osm
  host = 192.168.54.130
  dbname = osm

  [tablenames]
  places = osm_hamburg_places
  roads = osm_hamburg_roads_merged
  addresses = osm_hamburg_addresses
  postcodes = osm_hamburg_postcodes_union
  admin = osm_hamburg_admin
  
  [projection]
  srs = EPSG:900913

The config file will be loaded via the libary.
A preconfigured config can be found at `imposm.geocoder repository <https://bitbucket.org/quiqua/imposm.geocoder/src/default/imposm/geocoder/config/geocoder.ini>`_.

Geocode
_______

.. note::
 The API of the geocoder is very likely to change in the future and you should be aware of that.

Using the libary
~~~~~~~~~~~~~~~~

After you imported the data and configured the geocoder, you can use the libary for geocoding.
All you need to do is to import the functions responsible for the task::

  from imposm.geocoder.model import init_model, geocode
  from imposm.geocoder.config import load_config

You have to load the config file with the ``load_config(filename)`` function.
This function will return a ``Config`` object and the entries of your config file can be accessed as attributes. All tablenames will be stored in a dictonary and the database is stored in a formatted string::
  
  conf = load_config(filename)
  print conf.tablenames['addresses']
  >>> osm_hamburg_addresses
  
  print conf.database
  # 'dialect://user:passwort@host/dbname'
  >>> postgresql://osm:osm@192.168.54.130/osm
  
The ``Config`` object itself is then passed over to the ``init_model(conf)`` function.
Basically the database connection will be initialized and administrated with this funtion. In addtion the config will be used by an ORM to map the neccessary tables to classes and assign the tablenames.
You have to call this function once.

The ``geocode(params)`` function takes a dictionary with the parameters to geocode::

 params = {
  'city': None,
  'road': None,
  'country': None,
  'postcode': None,
  'housenumber': None,
 }
 
By default, all values are set to ``None``.
It is not yet possibile to pass over a single string containing an address like ``'place, road, housenumber'``. You have to assign each value seperatly.

The results will be returned as a dictionary but can be used as JSON/GeoJSON.
Each geocoded address, street, etc. will be represented by a point and has several metadata attached to it.

Console Application
~~~~~~~~~~~~~~~~~~~

In addition to the libary, the geocoder can be tested and used via the console::

  $ imposm-geocoder geocode --help
  usage: imposm-geocoder geocode [--config <config file>] [-r <road name>]
                                 [-n <housenumber>] [-p <postal code]
                                 [-c <city name>] [-x <two letter country code>]
                                 [--help]

  optional arguments:
    --config <config file>
    -r <road name>, --road <road name>
    -n <housenumber>, --housenumber <housenumber>
    -p <postal code, --postcode <postal code
    -c <city name>, --city <city name>
    -x <two letter country code>, --country <two letter country code>
    --help                show this help message and exit
  
You have to specify the config file for the geocoder. Furthermore if you want to geocode an address, you have to set the options ``-c <city name>``, ``-r <road name>`` and ``-n <housenumber>``.
The results will be returned as JSON and printed to the console.

For example::

  $ imposm-geocoder geocode --config path/to/geocoder.ini -c 'Oldenburg' -r 'Vahlenhorst' -n '33'
  [
    {
      "geometry": {
        "type": "Point", 
        "coordinates": [
          911983.314191282, 
          7012129.93616291
        ]
      }, 
      "type": "Feature", 
      "properties": {
        "query_type": "OSMQueryAddress", 
        "display_name": "Vahlenhorst 33, Oldenburg", 
        "address": {
          "city": "Oldenburg", 
          "name": null, 
          "country": null, 
          "street": "Vahlenhorst", 
          "postcode": null, 
          "type": "33", 
          "id": 540465517
        }
      }, 
      "bbox": [
        911883.314191282, 
        7012029.93616291, 
        912083.314191282, 
        7012229.93616291
      ]
    }
  ]
  

