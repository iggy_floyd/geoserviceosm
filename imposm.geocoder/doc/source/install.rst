Installation
============

Requirements
------------

Imposm.geocoder runs with Python 2.5, 2.6 and 2.7 and is tested on Linux and Mac OS X.

The following Python packages are required:

- `Imposm <http://imposm.org/>`_: OpenStreetMap data import tool
- `SQLAlchemy <http://sqlalchemy.org/>`_: Python SQL toolkit and Object Relational Mapper
- `GeoAlchemy <http://geoalchemy.org/>`_: GIS Support for SQLAlchemy

These Python packages will be installed automatically when you install imposm.geocoder with ``pip`` or ``easy_install`` (see below).

.. note:: Imposm itself depends on other packages. `Read the Imposm installation documentation <http://imposm.org/docs/imposm/latest/install.html>`_ for more information.


Installation
------------

Imposm.geocoder is registered at the `Python Package Index <http://pypi.python.org/pypi/imposm.geocoder>`_ and you can install it with ``pip`` or ``easy_install``.

::

  sudo aptitude install python-pip
  sudo pip install imposm.geocoder

You should now be able to start Imposm.geocoder::

  imposm-geocoder --help

virtualenv
~~~~~~~~~~

It is recommended to install Imposm.geocoder into a `virtual Python environment <venv>`_, especially if you are also running other Python based software.

On Ubuntu::

  sudo aptitude install python-virtualenv
  virtualenv venv
  venv/bin/pip install imposm.geocoder

You can then start Imposm.geocoder directly from your virtual environment::

  venv/bin/imposm-geocoder --help

.. _`venv`: http://pypi.python.org/pypi/virtualenv

