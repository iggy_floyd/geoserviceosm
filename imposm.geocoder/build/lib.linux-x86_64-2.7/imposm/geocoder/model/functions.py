from geoalchemy2 import Geometry
from geoalchemy2.functions import GenericFunction

class multiline_center(GenericFunction):
  name = 'multiline_center'
  type = Geometry
