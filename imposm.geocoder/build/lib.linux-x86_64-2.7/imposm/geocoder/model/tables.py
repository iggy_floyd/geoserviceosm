# This file is part of imposm.geocoder.
# Copyright 2012 Omniscale (http://omniscale.com)
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#    http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""SQLAlchemy ORM"""

from sqlalchemy import Table, Column, Integer, String, ForeignKey
from sqlalchemy.orm import mapper
from geoalchemy2 import Geometry
from imposm.geocoder.model import meta

class Address(object):
    def __init__(self, osm_id, name, housenumber, country, postcode, city, street, geometry):
        self.osm_id = osm_id
        self.name = name
        self.housenumber = housenumber
        self.country = country
        self.postcode = postcode
        self.city = city
        self.street = street
        self.geometry = geometry

class LookupPlace(object):
    def __init__(self, osm_id, lookup_name, name, type, z_order, population, geometry):
        self.osm_id = osm_id
        self.name = name
        self.type = type
        self.lookup_name = lookup_name
        self.z_order = z_order
        self.population = population
        self.geometry = geometry

class Admin(object):
    def __init__(self, osm_id, name, type, admin_level, geometry):
        self.osm_id = osm_id
        self.name = name
        self.type = type
        self.admin_level = admin_level
        self.geometry = geometry

class Postcode(object):
    def __init__(self, id, postcode, geometry):
        self.id = id
        self.postcode = postcode
        self.geometry = geometry

class UnifiedRoad(object):
    def __init__(self, id, name, osm_ids, geometry):
        self.id = id
        self.name = name
        self.osm_ids = osm_ids
        self.geometry = geometry

def map_config(config):
    address = Table('%s' % (config.tablenames['addresses']),
                    meta.metadata,
                    Column('osm_id', Integer, primary_key=True),
                    Column('name', String()),
                    Column('addr:housenumber', String()),
                    Column('addr:country', String()),
                    Column('addr:postcode', String()),
                    Column('addr:city', String()),
                    Column('addr:street', String()),
                    Column('geometry', Geometry('POINT', srid=config.srid))
                    )
    place = Table('%s' % (config.tablenames['places']),
                    meta.metadata,
                    Column('osm_id', Integer, primary_key=True),
                    Column('lookup_name', String()),
                    Column('name', String()),
                    Column('type', String()),
                    Column('z_order', Integer()),
                    Column('population', Integer()),
                    Column('geometry', Geometry('POINT', srid=config.srid))
                    )
    admin = Table('%s' % (config.tablenames['admin']),
                    meta.metadata,
                    Column('osm_id', Integer, primary_key=True),
                    Column('name', String()),
                    Column('type', String()),
                    Column('admin_level', Integer()),
                    Column('geometry', Geometry('POLYGON', srid=config.srid))
                    )
    postcode = Table('%s' % (config.tablenames['postcodes']),
                    meta.metadata,
                    Column('id', Integer, primary_key=True),
                    Column('postcode', String()),
                    Column('geometry', Geometry('POLYGON', srid=config.srid))
                    )
    road = Table('%s' % (config.tablenames['roads']),
                    meta.metadata,
                    Column('id', Integer, primary_key=True),
                    Column('name', String()),
                    Column('osm_ids', Integer()),
                    Column('geometry', Geometry('MULTILINESTRING', srid=config.srid))
                    )
    mapper(Address, address, properties={
                                'geometry': address.c.geometry,
                                'street': address.c.get('addr:street'),
                                'country': address.c.get('addr:country'),
                                'postcode': address.c.get('addr:postcode'),
                                'city': address.c.get('addr:city'),
                                'housenumber': address.c.get('addr:housenumber'),
                                'name': address.c.name,
                                'osm_id': address.c.osm_id})
    mapper(LookupPlace, place, properties={
                                'geometry': place.c.geometry,
                                'lookup_name': place.c.lookup_name,
                                'name': place.c.name,
                                'osm_id': place.c.osm_id,
                                'z_order': place.c.z_order,
                                'population': place.c.population,
                                'type': place.c.type})
    mapper(Admin, admin, properties={
                                'geometry': admin.c.geometry,
                                'name': admin.c.name,
                                'type': admin.c.type,
                                'osm_id': admin.c.osm_id,
                                'admin_level': admin.c.admin_level})
    mapper(Postcode, postcode, properties={
                                'geometry': postcode.c.geometry,
                                'postcode': postcode.c.postcode,
                                'id': postcode.c.id})
    mapper(UnifiedRoad, road, properties={
                                'geometry': road.c.geometry,
                                'name': road.c.name,
                                'id': road.c.id,
                                'osm_ids': road.c.osm_ids})