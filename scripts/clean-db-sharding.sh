#!/bin/bash

# run this as postgres user, eg:



set -xe
workers=(`ps auxw |  grep postgres | grep -- -D | grep worker | awk '{print $13}' | sed -e 's/.*worker\(.*\)/\1/'`)
for worker in "${workers[@]}"
 do
 sudo -u postgres /usr/lib/postgresql/9.4/bin/pg_ctl -D /var/lib/postgresql/9.4/worker${worker}   stop
 sudo rm -r /var/lib/postgresql/9.4/worker${worker}
 sudo rm -r /var/run/postgresql/worker${worker}
 done
set +x
