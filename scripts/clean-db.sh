#!/bin/bash

# run this as postgres user, eg:
# sudo -u postgres;  ./clean-db.sh



set -xe
dropdb osm
dropuser osm
set +x
