#!/usr/bin/env bash


#@author @2014 Igor Marfin [Unister Gmbh] <igor.marfin@unister.de>
#@description  Backup rotation system

_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"
_parent_dir="${_local_dir}/.."
_script_dir="${_local_dir}/../scripts"
_daemon_dir="${_local_dir}/../daemons"

## reading ini file for further initialization
source <(
grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
)



## Logging service initialization
LOG_TAG=UPDATE_SYSTEM;
export LOG_FILE="${_parent_dir}"/"${geoservice_logging_relative_path}";

if  [ -z  $BL_LEVEL  ]
then

	cd "${_script_dir}"
	. bashlog;
	cd - 2>&1 /dev/null;
fi




## define 'return' codes of different subroutines
declare -A CODES;
CODES[OK]=0;
CODES[ERROR]=1;


## daemon support
source ${_daemon_dir}/_oobash.sh


## daemons.cfg stores previous settings
_daemons_cfg="${_daemon_dir}/daemons.cfg";
if [ -f "${_daemons_cfg}" ]
then
    source ${_daemons_cfg}
fi

keys=("${!daemon_settings[@]}")

if [  ${#keys} -eq 0 ]
then

    _port_in=9880
    _port_out=9881
else
     new DAEMON daemonobj "UPDATE" "<>" 0 1 2 3 "<>";
    _port_in=$($daemonobj.restore_inprogport daemon_settings);
    _port_out=$($daemonobj.restore_outprogport daemon_settings);
fi





## define a trap function to be called to handle SIGIN, SIGQUIT, SIGTERM and SIGHUP signals
## this trap function is important to handle deadlocks in bktree service!
trap_function(){
                echo "Updating system is stopping now, wait...";
                _bashlog CRIT 'Updating system is stopping now, wait...';
                echo  "UPDATE_STOP"  | nc  -q1   localhost ${_port_in} 2> /dev/null;
                echo  ${CODES[ERROR]}   | nc  -q1   localhost  ${_port_out} 2> /dev/null;
                exit ${CODES[ERROR]};
}

trap "trap_function" SIGTERM SIGINT SIGQUIT SIGHUP SIGKILL

#main function
main(){

    _bashlog INFO 'Updating system has been started';


    # an error code to be returned
    local _err=${CODES[OK]};
    while [ true ]
    do
       local request=`nc -l  -p ${_port_in} 2>/dev/null`
      _bashlog DEBUG "request was $request " ;

        ## here; stop of the service
        if [ "$request" == "UPDATE_STOP" ]
            then
                _err=${CODES[OK]};
                 echo  $_err   | nc -q4  localhost ${_port_out} 2> /dev/null;
                _bashlog INFO 'main() has been killed....';
                return ${CODES[OK]};

	elif [ "$request" == "UPDATE_UPDATE" ]
        then
##            ${_script_dir}/update_maps.sh

            rm -r ${imposm_cache_dir}/* ## clean the cache
            ${_script_dir}/download_maps.sh
            ${_script_dir}/import_maps.sh
            echo  ${CODES[OK]} | nc   -q4   localhost ${_port_out} 2> /dev/null;

        else ## not supported operation
            echo  $_err | nc   -q1   localhost ${_port_out} 2> /dev/null;

            fi


    done


_bashlog INFO 'main() has stoped....';
return ${CODES[OK]};
}


main;

## do we need  to release the trap?
##trap - SIGINT SIGQUIT SIGTSTP SIGTERM SIGHUP SIGKILL

