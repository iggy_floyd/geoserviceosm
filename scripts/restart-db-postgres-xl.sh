#!/bin/bash



_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"
_parent_dir="${_local_dir}/.."

# reading ini file for further initialization
source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)




set -xe


cd "${_parent_dir}"/postgres-xl

[[ ! -d  _build2  ]] && echo "Please, install the Postgres-XL first. Tutorial is given in HOW-TO-XL" && exit 1

# 1) Kill all postgres instances
ps aux | grep _build2 | awk '{print $2}' | xargs -I {} sudo kill -15 {} || :
#ps aux | grep  -v grep | grep postgres | awk '{print $2}' | xargs -I {} sudo kill -9 {} ||:

datanodes=(`ls postgresql.conf.datanode* | sed -e 's/postgresql.conf.datanode//'`)

sleep 10
# 2) START of the cluster
sudo -u postgres _build2/bin/gtm  -D /var/lib/postgresql/data_gtm >logfile 2>&1 &
sleep 2
sudo -u postgres _build2/bin/gtm_proxy  -D /var/lib/postgresql/data_gtm_proxy >>logfile 2>&1 &
sleep 2
sudo -u postgres _build2/bin/postgres --coordinator -D /var/lib/postgresql/data_coord1  >>logfile 2>&1 &


workers=(`ps auxw |  grep postgres | grep -- -D | grep worker | awk '{print $13}' | sed -e 's/.*worker\(.*\)/\1/'`)

for datanode in "${datanodes[@]}"
 do
  _port=`cat postgresql.conf.datanode${datanode} | grep port | grep change | sed -e 's/\(.*\)#.*/\1/' | sed -e 's/.*=\(.*\)/\1/'`
  [[ -f  /var/lib/postgresql/data_datanode${datanode}/postmaster.pid ]] && sudo -u postgres rm /var/lib/postgresql/data_datanode${datanode}/postmaster.pid 
  sudo -u postgres _build2/bin/postgres --datanode -p $_port -D  /var/lib/postgresql/data_datanode${datanode} >>logfile 2>&1 &
  sleep 2
 done

set +x
