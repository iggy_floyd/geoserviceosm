#!/bin/bash




_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"
_parent_dir="${_local_dir}/.."

# reading ini file for further initialization
source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)




set -xe


cd "${_parent_dir}"/postgres-xl

[[ ! -d  _build2  ]] && echo "Please, install the Postgres-XL first. Tutorial is given in HOW-TO-XL" && exit 1



# clean the db
sudo -u postgres _build2/bin/dropdb "${database_name}" ||:
sudo -u postgres _build2/bin/dropuser "${database_user}" ||:


# 1) Kill all postgres instances
ps aux | grep _build2 | awk '{print $2}' | xargs -I {} sudo kill -15 {} ||:
#ps aux | grep postgres | awk '{print $2}' | xargs -I {} sudo kill -9 {} ||:

rm logfile ||:

datanodes=(`ls postgresql.conf.datanode* | sed -e 's/postgresql.conf.datanode//'`)

for datanode in "${datanodes[@]}"
 do
  sudo rm -r "/var/lib/postgresql/data_datanode${datanode}" ||:
 done
sudo rm -r /var/lib/postgresql/data_gtm ||: 
sudo rm -r /var/lib/postgresql/data_coord1 ||:
sudo rm -r /var/lib/postgresql/data_gtm_proxy ||:

set +x
