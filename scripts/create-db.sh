#!/bin/bash

# run this as postgres user, eg:
# sudo -u postgres  ./create_db.sh
_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"

# reading ini file for further initialization
source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)



set -xe
createuser --no-superuser --no-createrole --createdb "${database_user}"
createdb --template=template0   --locale=${utf8_language}.UTF-8 -E UTF-8  -O "${database_user}" "${database_name}"
createlang plpgsql "${database_name}" ||:
psql -d "${database_name}" -f "${postgis_path}/postgis.sql" 				# <- CHANGE THIS PATH
psql -d "${database_name}" -f "${postgis_path}/spatial_ref_sys.sql" 			# <- CHANGE THIS PATH
psql -d "${database_name}" -f /usr/local/lib/python2.7/dist-packages/imposm/900913.sql
echo "ALTER TABLE geometry_columns OWNER TO ${database_user};" | psql -d "${database_name}"
echo "ALTER TABLE spatial_ref_sys OWNER TO ${database_user};" | psql -d "${database_name}"
echo "ALTER USER ${database_user} WITH PASSWORD '${database_passwd}';" |psql -d "${database_name}"
echo "CREATE EXTENSION pg_trgm;" |psql -d "${database_name}"

echo "host	${database_name} 	${database_name}	127.0.0.1/32	md5" >> "${postgre_sql_path}/pg_hba.conf" 	# <- CHANGE THIS PATH
set +x
echo "Done. Don't forget to restart postgresql! via"
echo "sudo /etc/init.d/postgresql restart"
