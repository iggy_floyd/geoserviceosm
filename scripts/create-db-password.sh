#!/bin/bash

# run it as source creat-db-password.sh

_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"

# reading ini file for further initialization
source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)



set -xe


echo "${imposm_postgis_host}:${imposm_postgis_port}:${database_name}:${database_user}:${database_passwd}">$HOME/.pgpass
export PGPASSFILE=$HOME/.pgpass
chmod 0600 $PGPASSFILE


