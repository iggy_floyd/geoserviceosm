#!/bin/bash

_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"
_parent_dir="${_local_dir}/.."

# reading ini file for further initialization
source <(grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh)


set -xe

cd "${_parent_dir}"/postgresql_sharding

#  Creation of the cluster of workers

num_worker=(`ls postgresql.conf.worker* | sed -e 's/postgresql.conf.worker//'`)
for worker in "${num_worker[@]}"
 do
  echo "Initialization of the Worker-$worker"
  sudo mkdir "/var/lib/postgresql/9.4/worker${worker}"
  sudo chown postgres.postgres "/var/lib/postgresql/9.4/worker${worker}"
  sudo chmod 0700 "/var/lib/postgresql/9.4/worker${worker}"
  sudo -u postgres  /usr/lib/postgresql/9.4/bin/initdb  -D "/var/lib/postgresql/9.4/worker${worker}"
  sudo mkdir "/var/run/postgresql/worker${worker}/"
  sudo chown postgres.postgres  "/var/run/postgresql/worker${worker}/"
  sudo -u postgres cp "postgresql.conf.worker${worker}"  "/var/lib/postgresql/9.4/worker${worker}/postgresql.conf"
  sudo -u postgres /usr/lib/postgresql/9.4/bin/pg_ctl -D "/var/lib/postgresql/9.4/worker${worker}"   start

 done


#  Replace the configuration file of the MASTER by one with pg_shard support. Add the list of workers to the data directory
echo "Replace the configuration file of the MASTER by one with pg_shard support. Add the list of workers to the data directory"
sudo cp postgresql.conf /etc/postgresql/9.4/main/postgresql.conf
sudo -u postgres cp pg_worker_list.conf  /var/lib/postgresql/9.4/main
sudo /etc/init.d/postgresql restart 


# Create a user and a database and enable the extenstions
echo "Create a user and a database and enable the extenstions"

## first, workers
for worker in "${num_worker[@]}"
 do
 echo "Creating the database ${database_name}  and user ${database_user} for the Worker-$worker"
 _port=`cat postgresql.conf.worker${worker} | grep port | grep change | sed -e 's/\(.*\)#.*/\1/' | sed -e 's/.*=\(.*\)/\1/'`
 sudo -u postgres createuser --no-superuser --no-createrole --createdb "${database_user}" -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"
 sudo -u postgres createdb  --locale=${utf8_language}.UTF-8 -E UTF-8  -O "${database_user}" "${database_name}"  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"
 sudo -u postgres createlang  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  plpgsql "${database_name}" ||:
 echo "ALTER USER ${database_user} WITH PASSWORD '${database_passwd}';" | sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}"
 echo "CREATE EXTENSION pg_shard;" | sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}"
 echo "CREATE EXTENSION pg_trgm;"  | sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}"
 echo "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA pgs_distribution_metadata TO ${database_user}; " | sudo -u postgres  psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}"
 echo "GRANT USAGE ON SCHEMA pgs_distribution_metadata TO ${database_user}; " | sudo -u postgres  psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}"
 echo "GRANT ALL PRIVILEGES ON ALL SEQUENCES  IN SCHEMA pgs_distribution_metadata TO ${database_user}; " | sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}"

 sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}" -f "${postgis_path}/postgis.sql"  # <- CHANGE THIS PATH
 sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}" -f "${postgis_path}/spatial_ref_sys.sql"   # <- CHANGE THIS PATH
 sudo -u postgres psql  -h "/var/run/postgresql/worker${worker}/"  -p "${_port}"  -d "${database_name}" -f /usr/local/lib/python2.7/dist-packages/imposm/900913.sql


 sudo -u postgres /usr/lib/postgresql/9.4/bin/pg_ctl -D "/var/lib/postgresql/9.4/worker${worker}"   restart
 done

## MASTER, then
echo "Creating the database ${database_name}  and user ${database_user} for the Master"
sudo -u postgres createuser --no-superuser --no-createrole --createdb "${database_user}"
sudo -u postgres createdb --template=template0   --locale=${utf8_language}.UTF-8 -E UTF-8  -O "${database_user}" "${database_name}"
sudo -u postgres createlang plpgsql osm ||:
echo "ALTER USER ${database_user} WITH PASSWORD '${database_passwd}';" | sudo -u postgres psql -d "${database_name}"
echo "CREATE EXTENSION pg_trgm;"  | sudo -u postgres psql -d "${database_name}"
echo "CREATE EXTENSION pg_shard;" | sudo -u postgres psql -d "${database_name}"
echo "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA pgs_distribution_metadata TO ${database_user}; " | sudo -u postgres  psql -d "${database_name}"
echo "GRANT USAGE ON SCHEMA pgs_distribution_metadata TO ${database_user}; " | sudo -u postgres  psql -d "${database_name}"
echo "GRANT ALL PRIVILEGES ON ALL SEQUENCES  IN SCHEMA pgs_distribution_metadata TO ${database_user}; " | sudo -u postgres  psql -d "${database_name}"

## Add schemas of the 'spatial_ref_sys' Table
echo " Add schemas of the 'spatial_ref_sys' Table"
sudo -u postgres psql -d "${database_name}" -f "${postgis_path}/postgis.sql"  # <- CHANGE THIS PATH
sudo -u postgres psql -d "${database_name}" -f "${postgis_path}/spatial_ref_sys.sql"   # <- CHANGE THIS PATH
sudo -u postgres psql -d "${database_name}" -f /usr/local/lib/python2.7/dist-packages/imposm/900913.sql
echo "ALTER TABLE geometry_columns OWNER TO ${database_user};" | sudo -u postgres psql -d "${database_name}"
echo "ALTER TABLE spatial_ref_sys OWNER TO ${database_user};"  | sudo -u postgres psql -d "${database_name}"

echo "change access method to the database"

sudo sh -c " echo host ${database_name}        ${database_name}        127.0.0.1/32    md5 >> ${postgre_sql_path}/pg_hba.conf "
set +x
echo "Done. Don't forget to restart postgresql! via"
echo "sudo /etc/init.d/postgresql restart"
