#!/bin/bash
# Script to install Nominatim on Ubuntu
# Tested on 14.04 (View Ubuntu version using 'lsb_release -a') using Postgres 9.3
#

set -xe

_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"
_parent_dir="${_local_dir}/.."

# reading ini file for further initialization
source <(
	grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
)


# directory where all pbf files are stored
data_dir="${_parent_dir}/${localdata_relative_path}"


# create a cache dir
mkdir -p ${imposm_cache_dir} 2>/dev/null


places=(${place_osm})
for place in "${places[@]}"
 do
 echo "We are processing the place: $place"
 region_osm=$(dirname "${place}")
 _place_osm=$(basename "${place}")
 _file_pbf="${region_osm}/${_place_osm}-latest.osm.pbf"
 _file_pbf_md5="${_file_pbf}.md5"

 # import data
 [[  -f  ${data_dir}/${_file_pbf} ]] &&  
# imposm --cache-dir ${imposm_cache_dir} --merge-cache  --read --concurrency 1 --proj ${imposm_projection} --write --optimize -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}  --deploy-production-tables -m ${imposm_mapping}  ${data_dir}/${_file_pbf} 
# imposm --cache-dir ${imposm_cache_dir} --merge-cache  --read --concurrency 3 --proj ${imposm_projection} --write --optimize -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}  --deploy-production-tables -m ${imposm_mapping}  ${data_dir}/${_file_pbf} 
 imposm --cache-dir ${imposm_cache_dir} --merge-cache  --read --concurrency 3 --proj ${imposm_projection}  -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}  -m ${imposm_mapping}  ${data_dir}/${_file_pbf}  &&
# good!
 imposm --cache-dir ${imposm_cache_dir}  --concurrency 3  --proj ${imposm_projection} --write --optimize -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}  --deploy-production-tables -m ${imposm_mapping}  ${data_dir}/${_file_pbf} 

# imposm --cache-dir ${imposm_cache_dir} --merge-cache --concurrency 4  --proj ${imposm_projection} --write --optimize -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}  --deploy-production-tables -m ${imposm_mapping}  ${data_dir}/${_file_pbf} 
# imposm --cache-dir ${imposm_cache_dir} --merge-cache  --read --concurrency 1  --proj ${imposm_projection} --write --optimize -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}  --deploy-production-tables -m ${imposm_mapping}  ${data_dir}/${_file_pbf} 
# imposm --cache-dir ${imposm_cache_dir} --merge-cache  --read --concurrency 2 --proj ${imposm_projection} --write --optimize -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}   -m ${imposm_mapping}  ${data_dir}/${_file_pbf} 


 # create the 'schema_2' tables
 #[[  -f  ${data_dir}/${_file_pbf} ]] &&  
 #imposm-geocoder prepare  --proj ${imposm_projection}  -d ${database_name} -U ${database_user} -h ${imposm_postgis_host} -p ${imposm_postgis_port}   -m ${imposm_mapping}  --table-prefix "osm_"

 done

exit 0
set +x
