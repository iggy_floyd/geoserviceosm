#!/usr/bin/env bash
# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# a simple deployment script for this project


# logging service 'up'
LOG_TAG=DEPLOYMENT
export LOG_FILE=log/logs
if  [ -z  $BL_LEVEL  ]
then
	logsdir=$(pwd)/scripts
	cd $logsdir
	. $logsdir/bashlog
	cd - 2>&1 /dev/null
fi

# clean 
if [ -f Makefile ]
then
	_bashlog INFO 'Cleaning the environment from previous build...';
	make clean
fi


_bashlog INFO 'Start of the deployment...  ';

# prepare configuration of the service: use predefined values from template
make create_config
_bashlog INFO 'Setup of the configuration...  done';

# set the locale for the postgis and postgres: en_US.UTF-8
make set_locale
_bashlog INFO 'Set the locale for the postgis and postgres: en_US.UTF-8... done'

# set the .pgpass file for osm user in the PostgreSQL DB
source scripts/create-db-password.sh
_bashlog INFO 'Set the .pgpass file for osm user in the PostgreSQL DB...  done';

if [[ -n "${POSTGRESXL_BACKEND}" ]]
 then
 # set up the DB
  make create_db_postgres_xl
  _bashlog INFO 'Set up the DB with Postgres-XL backend...  done';
 else
  # set up the DB
  make create_db
  _bashlog INFO 'Set up the DB with PostgreSQL backend...  done';
  # do some optimization of the postgresql
  make optimization_postgresql
  _bashlog INFO 'Optmization of the PostgresSQL DB...  done';
fi


# apply patches to fix small bugs in the osmimport and osmimport.geocoder apis
make apply_fix
_bashlog INFO 'Apply patches to fix small bugs in the osmimport and osmimport.geocoder apis... done'

# apply patches to add support of the Postgres-XL in osmimport and osmimport.geocoder apis
[[ -n "${POSTGRESXL_BACKEND}" ]] &&  make apply_postgres_xl_fix && _bashlog INFO 'Apply patches to add support of the Postgres-XL in osmimport and osmimport.geocoder apis... done'
POSTGRESXL_BACKEND=''


# download osm maps for regions defined in the configs/config.ini
make download_maps
_bashlog INFO 'Download osm maps for regions defined in the configs/config.ini... done'

# import these maps to the DB
make import_maps
_bashlog INFO 'Import the maps to the DB... done'

# add a cron job for daily updating the OSM maps
make osm_update_cronjob_add
_bashlog INFO 'Add a cron job for daily updating the OSM maps... done'

# start daemons: address validation and updating services
make start_daemons
_bashlog INFO 'Start daemons for address validation and updating services... done'


# that's it
_bashlog INFO 'Deployment has been finished';



