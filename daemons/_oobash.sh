#!/bin/bash

# Oobash is a module which makes it possible to write
# pseudo-object-oriented bash-scripts.

# Copyright (C) 2010 Dominik Burgdörfer <dominik.burgdoerfer@googlemail.com>

# Oobash is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Oobash is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with oobash.  If not, see <http://www.gnu.org/licenses/>.



declare -A __OOB_OBJ_TABLE
declare -A __OOB_ADDR_TABLE


# Create new object without registering it in the object table.

# Arguments:
#    * unique objectname
#    * variable name to export object name to
#    * member declarations ...
new_metaobject()
{
    echo "new_metaobject: This function is deprecated. Use new_metaobject2" >&2

    local OBJ_NAME=$1
    shift

    local VARNAME=$1
    shift

    while ! [[ -z $1 ]]; do
        if [[ $1 == "function" ]]; then
            eval "$OBJ_NAME.$2() { $3 $OBJ_NAME \"\$@\"; }"
        elif [[ $1 == "var" ]]; then
            eval ${OBJ_NAME}_$2=$3
            eval "$OBJ_NAME.$2() { echo \"\$${OBJ_NAME}_$2\"; }"
            eval "$OBJ_NAME.set_$2() { ${OBJ_NAME}_$2=\$1; }"
        fi
        shift 3
    done

    eval $VARNAME=$OBJ_NAME
}


# Create new object without registering it in the object table.

# Arguments:
#    * unique objectname
#    * variable name to export object name to
#    * member declarations ...
new_metaobject2()
{
    local OBJ_NAME=$1
    shift

    local CLASS=$1
    shift

    local VARNAME=$1
    shift

    local SHIFT
    local VALUE_FIELD
    local CLASS_FIELDS=1

    while ! [[ -z $1 ]]; do
        if [[ $3 == "=" ]]; then
            SHIFT=4
            VALUE_FIELD=$4
        else
            SHIFT=2
            VALUE_FIELD=  # empty
        fi

        if [[ $1 == "function" ]]; then
            [[ -z $VALUE_FIELD ]] && VALUE_FIELD=${CLASS}::$2
            eval "$OBJ_NAME.$2() { $VALUE_FIELD $OBJ_NAME \"\$@\"; }"
        elif [[ $1 == "declare" ]]; then
            eval ${OBJ_NAME}_$2=$VALUE_FIELD
            eval "$OBJ_NAME.$2() { echo \"\$${OBJ_NAME}_$2\"; }"
            eval "$OBJ_NAME.set_$2() { ${OBJ_NAME}_$2=\$1; }"
        else
            echo -e "oobash: Syntax error in class-field $CLASS_FIELDS in class $CLASS,
\texpected function or declare keyword" >&2
            return 1
        fi

        (( ++CLASS_FIELDS ))
        shift $SHIFT
    done

    eval $VARNAME=$OBJ_NAME
    return 0
}


# Create new object and register it in the object table

# Arguments:
#    * Variable name of the class array
#    * Variable name to export object name to
#    + (optional) arguments to constructor.
new()
{
    local CLASS=$1
    shift

    local VARNAME=$1
    shift

    local i

    # Increment class id number.
    if [[ -z ${__OOB_OBJ_TABLE[$CLASS]} ]]; then
        __OOB_OBJ_TABLE[$CLASS]=1
    else
        ((__OOB_OBJ_TABLE[$CLASS] += 1))
    fi


    # Generate unique object-name.
    local OBJ_NAME="${CLASS}_Object_id_${__OOB_OBJ_TABLE[$CLASS]}"

    # Register object-name.
    __OOB_ADDR_TABLE[$CLASS]="${__OOB_ADDR_TABLE[$CLASS]}:$OBJ_NAME:"


    # Create new object.
    eval new_metaobject2 $OBJ_NAME $CLASS $VARNAME \"\${$CLASS[@]}\"

    # Call constructor.
    [[ $(type -t $OBJ_NAME.__new__) == function ]] && $OBJ_NAME.__new__ "$@"
}


# Deletes All references to the object
# and calls the destructor if it exists.

# Arguments:
#    * A reference to an object
delete()
{
    local CLASSNAME=$(echo $1|sed -r 's/_Object_id_[0-9]+$//')

    __OOB_ADDR_TABLE[$CLASSNAME]=$(echo "${__OOB_ADDR_TABLE[$CLASSNAME]}"|sed -r "s/:$1://")

    if [[ -z ${__OOB_ADDR_TABLE[$CLASSNAME]} ]]; then
        unset __OOB_ADDR_TABLE[$CLASSNAME]
    fi

    # Check for destructor and call it if one is existent.
    [[ $(type -t $1.__delete__) == function ]] && $1.__delete__
}


# Deletes all references to the objects of all or
# specific classes.

# Arguments:
#    + (optional) Classnames ...
delete_all()
{
    local i
    local j

    if [[ -z $1 ]]; then
        # Loop through all registered objects and delete them
        for i in "${__OOB_ADDR_TABLE[@]}"; do
            local a=$(echo "$i"| \
                awk 'BEGIN { RS = ":+" } /^.+$/ {print $1" "}')

            for j in $a; do
                delete $j
            done
        done
    else
        for i; do
            local str=${__OOB_ADDR_TABLE[$i]}
            local a=$(echo "$str"| \
                awk 'BEGIN { RS = ":+" } /^.+$/ {print $1" "}')

            for j in $a; do
                delete $j
            done
        done
    fi
}



#### Daemon definitions!
## class DAEMON

DAEMON=(
    # Constructor.    
    function __new__

    # Destructor
    function __delete__

    # public methods
    function get_name 
    function get_uuid
    function get_cmd
    function get_indaemonport
    function get_outdaemonport
    function get_inprogport
    function get_outprogport
    function get_stopword
    function store
    function restore_cmd
    function restore_uuid
    function restore_indaemonport
    function restore_outdaemonport
    function restore_inprogport
    function restore_outprogport
    function restore_stopword

    #variable
    declare name
    declare uuid 
    declare cmd
    declare stopword
    declare indaemonport
    declare outdaemonport
    declare inprogport
    declare outprogport

)

## Daemon implementation

DAEMON::__new__(){
    local self=$1
    shift

    $self.set_name "$1"
    ##$self.set_uuid "$(cat /dev/urandom | tr -dc 'a-z0-9' | fold -w 8 | head -n 1)"
    $self.set_uuid "$2"
    $self.set_indaemonport "$3"
    $self.set_outdaemonport "$4"
    $self.set_inprogport "$5"
    $self.set_outprogport "$6"
    $self.set_stopword "$7"
    $self.set_cmd "$8"
}


DAEMON::__delete__(){
    local self=$1
    shift
    #echo "destructor of the object $($self.name) is called";
}


DAEMON::get_name()
{
# Get self reference.
    local self=$1
    shift    
    echo "$($self.name)"
}


DAEMON::get_uuid()
{
# Get self reference.
    local self=$1
    shift    
    echo "$($self.uuid)"
}



DAEMON::get_cmd()
{
# Get self reference.
    local self=$1
    shift 
    echo "$($self.cmd)"       
}

DAEMON::get_indaemonport()
{
# Get self reference.
    local self=$1
    shift
    echo "$($self.indaemonport)"
}

DAEMON::get_outdaemonport()
{
# Get self reference.
    local self=$1
    shift
    echo "$($self.outdaemonport)"
}

DAEMON::get_inprogport()
{
# Get self reference.
    local self=$1
    shift
    echo "$($self.inprogport)"
}

DAEMON::get_outprogport()
{
# Get self reference.
    local self=$1
    shift
    echo "$($self.outprogport)"
}

DAEMON::get_stopword()
{
# Get self reference.
    local self=$1
    shift
    echo "$($self.stopword)"
}

DAEMON::store()
{
# Get self reference.
    local self=$1
    shift 
    eval "$1[$($self.name)]=\"$($self.uuid) $($self.stopword) $($self.indaemonport) $($self.outdaemonport) $($self.inprogport) $($self.outprogport) $($self.cmd)\""
}


DAEMON::restore_uuid()
{
# Get self reference.
    local self=$1
    shift     
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);      
    echo "${_arr[@]:0:1}"    
}

DAEMON::restore_stopword()
{
# Get self reference.
    local self=$1
    shift
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);
    echo "${_arr[@]:1:1}"
}


DAEMON::restore_indaemonport()
{
# Get self reference.
    local self=$1
    shift
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);
    echo "${_arr[@]:2:1}"
}

DAEMON::restore_outdaemonport()
{
# Get self reference.
    local self=$1
    shift
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);
    echo "${_arr[@]:3:1}"
}

DAEMON::restore_inprogport()
{
# Get self reference.
    local self=$1
    shift
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);
    echo "${_arr[@]:4:1}"
}

DAEMON::restore_outprogport()
{
# Get self reference.
    local self=$1
    shift
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);
    echo "${_arr[@]:5:1}"
}

DAEMON::restore_cmd()
{
# Get self reference.
    local self=$1
    shift
    local _arr="\${$1[$($self.name)]}";
    _arr=(`eval "expr \"$_arr\" "`);
    echo "${_arr[@]:6:${#_arr[@]}}"
}