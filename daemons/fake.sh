#!/usr/bin/env bash

source _oobash.sh





## main parameters of the script
_setuppath=`pwd`;

## daemons.cfg stores previous settings
_daemons_cfg="${_setuppath}/daemons.cfg";
if [ -f "${_daemons_cfg}" ]
then
    source ${_daemons_cfg}
fi

keys=("${!daemon_settings[@]}")

if [  ${#keys} -eq 0 ]
then

    _port_in=9880
    _port_out=9881
else
    new DAEMON daemonobj "FAKE" 0 1 2 3 "<>";
    _port_in=$($daemonobj.restore_inprogport daemon_settings);
    _port_out=$($daemonobj.restore_outprogport daemon_settings);
fi
# define a trap function to be called to handle SIGIN, SIGQUIT, SIGTERM and SIGHUP signals
trap_function(){
        echo  "FAKE_STOP"  | nc  -q1   localhost ${_port_in} 2> /dev/null;
        echo  1 | nc  -q1   localhost ${_port_out} 2> /dev/null;
        exit 0;
}

trap "trap_function" SIGTERM SIGINT SIGQUIT SIGHUP SIGKILL


## possible keywords, with which the system operates,:
##   FAKE_STOP --> to kill the service
##   FAKE_CHECK --> to check if the service is live

## Example of use: echo -ne 'FAKE_STOP' | nc -q1  localhost [_port_in]
## Example of use: echo -ne 'FAKE_CHECK' | nc -q1  localhost  [_port_in]


main(){
    [[  (  -z `which nc` ) ]] && return 1



    while [ 1 ]
    do

        local request=`nc -l  -p ${_port_in} 2>/dev/null`;

        # here; stop of the service
        if [ "$request" == "FAKE_STOP" ]
        then
            echo  0   | nc -q1  localhost ${_port_out} 2> /dev/null;
            return 0;
        elif [ "$request" == "FAKE_CHECK" ]
        then
	        echo "I am a Fake and  I am live!"  | nc -q4  localhost ${_port_out} 2> /dev/null;

        else # not supported operation
            echo  1 | nc   -q1   localhost ${_port_out} 2> /dev/null;

	    fi
    done
}

main;
trap - SIGINT SIGQUIT SIGTSTP SIGTERM SIGHUP SIGKILL