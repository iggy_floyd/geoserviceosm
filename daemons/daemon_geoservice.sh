#!/usr/bin/env bash

# Daemon control script for the Geocoding process
# ==============================================================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   daemon_geoservice.sh
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

# This script controls the 'geoservice' daemon in the system.
#

# Usage
# -------------------------------
#
# ./daemon_geoservice.sh <start|stop|status>



# The code of the script
# -----------------------
#
# ::



export DAEMON_NAME=GEOSERVICE
./daemon.sh $@
