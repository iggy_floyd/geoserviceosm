#!/usr/bin/env bash

# Setup of the daemons
# ==============================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   setup.daemons.sh
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

# The script initialize daemons which will be operated by ``daemon.sh``
# 

# Usage
# -------------------------------
#
# ./setup.daemons.sh


# OOBash -  oo-style framework for bash 4
# -----------------------------------------------------
#
# ::


source _oobash.sh 

# Make sure to delete all objects at end of program
trap 'delete_all' TERM EXIT INT


# The shell code 
# -----------------------
#
# ::

_abs_path_to_this_file=$(readlink -f "$0")
_local_dir=$(dirname "$_abs_path_to_this_file")
_config_dir="${_local_dir}/../configs"
_parent_dir="${_local_dir}/.."

## reading ini file for further initialization
source <(
	grep = "${_config_dir}/config.ini" |  grep -v ';' | sed 's/ *= */=/g' | xargs -I {}   echo "echo -n {} |  sed -e 's/\(.*\)=\(.*\)/\1/;s/\./\_/g';echo -n =; echo  {} |  sed -e 's/\(.*\)=\(.*\)/\"\2\"/'" | sh
)


_setuppath="${_local_dir}"

## declare settings array
declare -A daemon_settings # please note, that associative array can be created in the bash with version >4.0

## config.ini stores previous settings 
_config_ini="${_setuppath}/daemons.cfg"
if [ -f "$_config_ini" ] 
then
    source ${_config_ini}
fi



## settings: store the DAEMON NAME, a corresponded unique identity,ports and a run command
GEOSERVICECMD="${_parent_dir}"/"${geoservice_relative_path}"
[ ! ${settings[GEOSERVICE]+abc} ] && {
    new DAEMON daemon1 "GEOSERVICE" "3mxgsora"  9740 9741 9742 9743 "GEOSERVICE_STOP"  "$GEOSERVICECMD";
    $daemon1.store daemon_settings;
    }



## settings: store the DAEMON NAME, a corresponded unique identity, ports and  a run command
UPDATECMD="${_parent_dir}"/"${geoservice_updater_relative_path}"
## Please use 9770 9771 for prog ports! in backupper
[ ! ${settings[BACKUP]+abc} ] && {
    new DAEMON daemon2 "UPDATE" "rbgol16f"  9750 9751 9770 9771 "UPDATE_STOP"  "$UPDATECMD";
    $daemon2.store daemon_settings;
    }


## here put another daemon definitions for more services you want to run via daemon.sh
##
## ...

## some fake settings
## please, don't remove it. It defines the default behavior of the daemon.sh script.

## settings: store the DAEMON NAME, a corresponded unique identify and a run command
[ ! ${settings[FAKE]+abc} ] && 
{
   new DAEMON daemon5 "FAKE" "7b7q95xw" 9790 9791 9792 9793 "FAKE_STOP" "`pwd`/fake.sh"
   $daemon5.store daemon_settings
}



## store settings to the file
declare -p  daemon_settings > ${_config_ini};
