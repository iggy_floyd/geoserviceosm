#! /usr/bin/env python
# -*- coding: utf-8 -*-

# SimilarityCalculator
# ==============================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   SimilarityCalculator.py
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#


'''
   SimilarityCalculator
'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::

from Logger import mylogging 
import sys 
from  Levenshtein import StringMatcher
from  Levenshtein._levenshtein import *
import numpy as np

# `translate(x)`
# ----------------------
# The function `translate(x)`  translates the string `x` to lower case and replaces all umlauts by ASCII symbols
#
# ::

def translate(x,encoding='utf-8'):
		    try:
  		     x=unicode(x).lower()
                    except Exception,e:
                     x=x.decode(encoding).lower()

                    table = {
                        0xe4: u'ae', # a diaeresis
                        ord(u'ö'): u'oe',
                        ord(u'ü'): u'ue',
                        ord(u'ß'): u'ss',
                        0xe1: u'a',     # a acute 'á' => 'a'
                        0xe0: u'a',     # a grave 'à' => 'a'
                        0xe2: u'a',     # a circumflex  'â' => 'a'
                        0xe3: u'a',     # a tilde 'ã' => 'a'
                        0xe7: u'c',     # c cedilla 'ç' => 'c'
                        0xe9: u'e',     # e acute 'é' => 'e'
                        0xe8: u'e',     # e grave 'è' => 'e'
                        0xea: u'e',     # e circumflex 'ê' => 'e'
                        0xed: u'i',     # i acute 'í' => 'i'
                        0xec: u'i',     # i grave 'ì' => 'i'
                        0xee: u'i',     # i circumflex 'î' => 'i'
                        0xef: u'i',     # i diaeresis 'ï' => 'i'
                        0xf1: u'n',     # n tilde 'ñ' => 'n'
                        0xf2: u'o',     # o grave 'ò' => 'o'
                        0xf3: u'o',     # o acute 'ó' => 'o'
                        0xf5: u'o',     # o tilde   'õ' => 'o'
                        0xf4: u'o',     # o circumflex 'ô' => 'o'
                        0xf6: u'o',     # o diaeresis 'ï' => 'o'
                        0xf9: u'u',     # u grave 'ù' => 'u'
                        0xfa: u'u',     # u acute 'ú' => 'u'
                        0xfb: u'u',     # u circumflex 'û' => 'u'
                        0xfd: u'y',     # y acute 'ý' => 'y'
                        ord(u'à'): u'a',
                        ord(u'ò'): u'o',
                        ord(u'ń'): u'n',

                    }
                    return x.translate(table).encode('ascii', 'ignore')


# SimilarityCalculator
# ------------------------
# The class is responsible for calculation of the similarity between two strings
# :translate:

class SimilarityCalculator(object):
 '''The class is responsible for calculation of the similarity between two strings'''

 def __call__(self,str1,str2,encoding='utf-8'):
   ''' calculates the similarity of strings '''
   str1,str2=translate(str1,encoding),translate(str2,encoding)
   dist=StringMatcher.StringMatcher(None,str1,str2).distance()
#   print "dist between %s and %s = %d"%(str1,str2,dist)
#   print "inverse similarity between %s and %s = %d"%(str1,str2,float(dist)/float( min([len(str1),len(str2)])))
   if (len(str1) == 0 ) or (len(str2) == 0): 
    return 0.0
   else:
#    return  1.0 - min(1.0,float(dist)/float( min([len(str1),len(str2)]) )) 
    return  1.0 - min(1.0,float(dist)/float( max([len(str1),len(str2)]) )) 


 def address_similarity(self,addr1,addr2,encoding='utf-8'):
  ''' addr1 and addr2 should have the following form:
      street;housenumber;zipcode;city
  '''

  def  myraise(e): raise ValueError(e)
  addr1,addr2=translate(addr1,encoding).split(';'),translate(addr2,encoding).split(';')
  
  assert len(addr1) == len(addr2) or myraise('DIFFERENT_ADDRESSES_FORMATS')
  assert len(addr1) == 4 or myraise('WRONG_ADDRESS_FORMAT')
 
  similarities = map(lambda x: self.__call__(x[1],addr2[x[0]],encoding='utf-8'), enumerate(addr1))
  similarities_squared = map(lambda x: self.__call__(x[1],addr2[x[0]],encoding='utf-8')**2., enumerate(addr1))
  return similarities,np.sqrt(np.average(similarities_squared)),min(similarities)


# The test suite of the SimilarityCalculator.py
# -------------------------------------
#
#  .. code-block:: bash
#
#                  vagrant@FPS-Fraud-Classifier2:~/project/lib> python SimilarityCalculator.py
#
# ::


if __name__ == "__main__":
        
    
    # Test of the translate
    unicode_str="ßäü"
    print "'original string': %s, 'translated string': %s"%(unicode_str,translate(unicode_str))
    UNICODE_STR_UPPER="ẞÄÜ"
    print "'original string': %s, 'translated string': %s"%(UNICODE_STR_UPPER,translate(UNICODE_STR_UPPER))
    assert translate(UNICODE_STR_UPPER) == translate(unicode_str)
    
    # Test of the similarity calculator:
    address1 =  '''Straubingr Str., 12,28219,bremen'''
    address2 =  u'Straubinger Stra\u00dfe 12,28219,Bremen'
    print "address obtained from OSM after translation:",translate(address2)        
    print "similarity of the addresses:",SimilarityCalculator()(address1,address2)
    assert SimilarityCalculator()(address1,address2) >0.70

    print "Validation of the missed leading zero in the zipcode;",SimilarityCalculator()("9116","09116")

    # Test of the address_similarity
    address1 = '''Straubinger Str.;12;28219;bremen'''
    address2 =  u'Straubinger Stra\u00dfe;12;28219;Bremen'
    print SimilarityCalculator().address_similarity(address1,address2)

    
