#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Best Address Finder
# ==============================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   BestAddress.py
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#

'''
   BestAddress is responsible to search for the best address candidate among results given by fuzzy search in the OSMAddress Tables
'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::

## no requirements

# BestAddress
# ------------
# The class BestAddress presents the algorithm to choose the best candidate among corresponded validated addresses
#
# ::


class BestAddress(object):
 ''' find the best addresses'''
 
 _best=None



# `__call__(self,addresses,min_city_similarity=0.90,min_zipcode_similarity=0.80,min_housenumber_similarity=0.80)`
# --------------------------------------------------------------------------------------------------------------------------------------
# 1) selects out addresses those similarities to the requested addresses is low than thresholds in the function call
# 2) finds the best candidate with the highest 'total_similarity'
#
# =============== ================================== ================================================================================================
# :param:         addresses (dict)                    an address returned by osm_geocoder_service.py
# :param:         min_city_similarity (float)         a threshold for similarity of the city in the address
# :param:         min_zipcode_similarity (float)      a threshold for similarity of the zipcode in the address
# :param:         min_housenumber_similarity (float)  a threshold for similarity of the building number in the address
# =============== ================================== ================================================================================================
#
# ::


 def __call__(self,addresses,min_city_similarity=0.90,min_zipcode_similarity=0.80,min_housenumber_similarity=0.50):
  for addr in addresses:
   try:
    if (addr['similarity_zipcode']<min_zipcode_similarity) or (addr['similarity_city']<min_city_similarity) or (addr['similarity_housenumber']<min_housenumber_similarity): continue
    if (BestAddress._best is None):    
     BestAddress._best=addr
     continue
   
    if (BestAddress._best['total_similarity']<addr['total_similarity']): 
     BestAddress._best=addr

   except:
    continue

  
 def __repr__(self):
  if BestAddress._best:
   return "%r"%BestAddress._best
  else:
   return "%r"%{} 
 
