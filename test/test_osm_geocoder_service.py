#! /usr/bin/env python
# -*- coding: utf-8 -*-

import jsonrpclib

import json
from time import sleep,time
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__)))+'/lib/')
from Logger import mylogging
from settings import  mylogging_threshold,geocoding_host,geocoding_server_port
mylogging.thresholdVerbosity= mylogging_threshold

from BestAddress import BestAddress
import time

import sys 

import argparse

# example of the run:
# ./test_osm_geocoder_service.py  --host "http://127.0.0.1:8095"
parser = argparse.ArgumentParser(description="Test of OSMGeoLocator")
parser.add_argument('--host', action="store", dest="host", type=str,required=True)
args = vars(parser.parse_args())
_geocoding_host=geocoding_host
if len(args['host'])>0:
 _geocoding_host=args['host'] 

print _geocoding_host
s = jsonrpclib.Server(_geocoding_host)
similarity_limit=0.99
similarity_limit=0.92
#similarity_limit=0.5
#similarity_limit=0.1
similarity_limit=1.-similarity_limit
print 'Test of the OSM Geocoder Service'
selector=BestAddress()

mylogging( "\n"*5)
mylogging( "you can stop the geo location service by running the command:")
mylogging( "\n"*2)
mylogging( "ps aux | grep osm_geocoder_service | awk '{print $2}' | head -1 | xargs -I {}  kill  {}")


start = time.time()
print '\n\n'
address='Straubinger Str:;6;28219;Bremen;DE'
address="Straubinger Str.;6;28219;Bremen;DE"
print address,'\n\n'

addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  '%s' at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
#print s.find_address_v2(*address.split(';'))
 mylogging('processed for %2.3f'%(time.time()-start))

print '\n\n'

start = time.time()
address='Straubinger Str.;13;28219;Bremen;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!", addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  '%s' at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
 mylogging('processed for %2.3f'%(time.time()-start))
#'''
print '\n\n'

start = time.time()
address='traubiner Str.;13;28219;Bremen;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!", addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  '%s' at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))
#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'

start = time.time()
address=u'---. 53CARL-FRIEDRICH-GAUß-STR.;53;28357;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!", addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'



start = time.time()
address=u'CAL-FRIEDRICH-GAUß-STR.;53;28355;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!", addresses_found
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'


start = time.time()
address=u'AACHENER STR.;9;28327;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
#print addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 mylogging('processed for %2.3f'%(time.time()-start))

 print "Found so far",selector

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'


start = time.time()
address=u'ACHTERDIEK,;43;28359;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
#mylogging('processed for %2.3f'%(time.time()-start))
print "FOUND!!!", addresses_found
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 mylogging('processed for %2.3f'%(time.time()-start))
 print "Found so far",selector

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'
	

start = time.time()
address=u'AM FASANENPAD;26;28355;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!", addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'

##sys.exit(1)

start = time.time()
address=u'ZSCHOERNERSTRASSE;10A;28779;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!", addresses_found
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))
#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'


start = time.time()
address=u'ADMIRALSTRASSE;156;28215;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
#print addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'

start = time.time()
address=u'AM LEHESTER DEICH;84 P;28357;BREMEN;DE'
print address,'\n\n'
BestAddress._best=None
addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
#addresses_found=s.find_address_v2(*address.split(';'))
print "FOUND!!!  ", addresses_found
#mylogging('processed for %2.3f'%(time.time()-start))
#'''
addresses_found=addresses_found['result']
for addr in addresses_found['addresses']:
 print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address,addr.keys()[0])
 print addr.values()[0]
 #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
 selector(addr.values()[0])
 print "Found so far",selector
 mylogging('processed for %2.3f'%(time.time()-start))

#print s.find_address(*(address.split(';')+[similarity_limit]))
#print s.find_address_v2(*address.split(';'))
#'''
print '\n\n'

#sys.exit(1)



lines = [line.rstrip('\n') for line in open('Bremen_addresses_revised_v2.csv')]
for address in lines:
 #time.sleep(0.7)
 start = time.time()

 print address,'\n\n'
 #print >>open('address_validation.log','a'),address,'\n\n'
 BestAddress._best=None
 addresses_found=s.find_address(*(address.split(';')+[similarity_limit]))
 addresses_found=addresses_found['result']
 print "FOUND!!!", addresses_found
 #addresses_found=s.find_address_v2(*address.split(';'))
 #print addresses_found
 #mylogging('processed for %2.3f'%(time.time()-start))
 #'''
 for addr in addresses_found['addresses']:
  print "\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address.decode('utf-8'),addr.keys()[0])
  #print >>open('address_validation.log','a'),"\n\nSearching for the best Candidate of  %s at '%s':\n\n"%(address.decode('utf-8'),addr.keys()[0])

  #selector(addr.values()[0],min_city_similarity=-0.90,min_zipcode_similarity=-0.80)
  selector(addr.values()[0])
  print "Found so far",selector
  #print >>open('address_validation.log','a'),"Found so far",selector
  mylogging('processed for %2.3f'%(time.time()-start))

 #print s.find_address(*(address.split(';')+[similarity_limit]))
 #print s.find_address_v2(*address.split(';'))
 #'''
 print '\n\n'
 #print >>open('address_validation.log','a'),'\n\n'
 

print "\n"*5
print "you can stop the OSM Geocoder Service by running the command:"
print "\n"*2
print "ps aux | grep osm_geocoder_service | awk '{print $2}' | head -1 | xargs -I {}  kill  {}"
#print s.close_service()

