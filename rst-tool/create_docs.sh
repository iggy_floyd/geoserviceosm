#!/usr/bin/env bash


# main function
main() {

[[ $# -lt 3 ]] &&
(echo "Error: <WRONG_NUMBER_ARGUMENTS>"; echo; echo "Usage: $0 file.rst category description";echo; echo " ./create_docs.sh demo.txt 'demo' 'A simple demo'"; echo; echo )     &&
 return 1;


local _file="$1";
local _category="$2";
local _description="$3";
local _file_base="${_file/.*/}";
_file_base=`basename ${_file_base}`
local _texinfo_ext='texi';
local _info_ext='info';
local _path_to_tool=`dirname $0`;

# it produces the the *.texi file
"${_path_to_tool}"/./rst2texinfo.py --texinfo-filename=${_file_base}.${_info_ext} --texinfo-dir-entry=$_file_base --texinfo-dir-category=$_category \
 --texinfo-dir-description=$_description $_file ${_file_base}.${_texinfo_ext};
local _res=$?;


#to build html
makeinfo --force --no-split --html ${_file_base}.${_texinfo_ext}; 
local _res=$? || $_res;

#to build README
makeinfo --no-split ${_file_base}.${_texinfo_ext} --no-headers > README;
_res=$? || $_res;

rm ${_file_base}.${_texinfo_ext} ;
_res=$? || $_res; 

return $_res;
}


main $@
