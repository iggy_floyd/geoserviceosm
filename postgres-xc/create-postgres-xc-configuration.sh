#!/bin/bash

TARGET_DIR_CONF=`pwd`/postgres-xc-config/
TARGET_DIR=/home/vagrant/project/postgres-xl/_build2/
DATA_DIR=$TARGET_DIR/data
#mkdir -p $DATA_DIR


# definition the type of the server (oltp is recommended)
#
# web = web application backend
#       1) DB smaller than RAM
#       2) 90% or more simple read queries
# oltp = online transaction processing
#       1) db slightly larger than RAM, up to 1 TB
#       2) 20-40% small data write queries
#       3) some long transactions
# dw = data warehousing
#       1) large to huge databases (100 GB to 100 TB)
#       2) large complex report queries
#       3) large bulk loads of data
#       4) also called "Decision Support" or "Business Intelligence"  


_type=oltp

echo "#\tPostgresql configuring based on system memory and recommended formulae"



if [ ! `whoami` = 'root' ]; then
        echo "This script needs to run as root because"
        echo "it alters shm{max,mni,all}"
        exit
fi

# Check if the 'dedicated' parameter has been supplied
if [ -z "$2" ]; then
    echo -n "Will this machine be dedicated (i.e. PostgreSQL is the only active service)? (y/n) "
    read dedicated
else
    dedicated=$2
fi


# These two are taken from performance-whack-a-mole (see link in header comments)
SHARED_BUFFER_RATIO=0.25
EFFECTIVE_CACHE_RATIO=0.67 

if [ "$_type" = "web" ]; then # web backend server 
        NUM_CONN=400
        WORK_MEM=512 # kB
        CHECKPOINT_SEG=8
        MAINT_WORK_MEM=128MB
elif [ "$_type" = "oltp" ]; then # online transaction processing
        if [ $dedicated = 'y' ]; then
                NUM_CONN=50
                WORK_MEM=8192 # kB
        else
                NUM_CONN=200
                WORK_MEM=2048 # kB
        fi
        CHECKPOINT_SEG=16
        MAINT_WORK_MEM=128MB
elif [ "$_type" = "dw" ]; then # data warehousing
        NUM_CONN=100
        WORK_MEM=131072 # kB
        CHECKPOINT_SEG=64
        MAINT_WORK_MEM=1024MB
fi

OS_TYPE=`uname -s`


### LINUX
if [ "$OS_TYPE" = "Linux" -o "$OS_TYPE" = "GNU/Linux" ]; then

        SYSCTL_KERNEL_NAME="kernel"
        MAX_MEM_KB=`grep MemTotal /proc/meminfo | sed -e 's/^[^0-9]*//' | cut -d' ' -f1`
        OS_PAGE_SIZE=`getconf PAGE_SIZE`

### OPENBSD
elif [ "$OS_TYPE" = "OpenBSD" ]; then

        SYSCTL_KERNEL_NAME="kern.shminfo"
        MAX_MEM_KB=$(echo "scale=0; `dmesg | grep \"real mem\" | cut -d\"=\" -f2 | cut -d\"(\" -f1`/1024" | bc -l ) # convert to kB
        OS_PAGE_SIZE=`sysctl hw.pagesize | cut -d'=' -f2`

### UNKNOWN?
else
        echo "$OS_TYPE isn't supported"
        exit
fi

# make sure work_mem isn't greater than total memory divided by number of connections...
WORK_MEM_KB=$(echo "scale=0; $MAX_MEM_KB/$NUM_CONN" | bc -l)
if [ $WORK_MEM_KB -gt $WORK_MEM ]; then
        while [ $WORK_MEM -lt $WORK_MEM_KB ]; do
                WORK_MEM_TEMP=$(echo "scale=0; $WORK_MEM*2" | bc -l)
                if [ $WORK_MEM_TEMP -lt $WORK_MEM_KB ]; then
                        WORK_MEM=$(echo "scale=0; $WORK_MEM*2" | bc -l)
                else
                        WORK_MEM_KB=0
                fi
        done     
        WORK_MEM_KB=$WORK_MEM; 
fi
WORK_MEM=$(echo "scale=0; $WORK_MEM_KB/1024" | bc -l)MB

echo
echo
echo "#\tConfiguring for system type: $OS_TYPE, max memory: $MAX_MEM_KB kB, page size: $OS_PAGE_SIZE bytes, working memory ${WORK_MEM}."
echo 
echo

# OS settings
HOSTNAME=`hostname`

# shm{mni,all,max} are critical to PostgreSQL starting.  
# They must be high enough for these settings:
#       max_connections
#       max_prepared_transactions
#       shared_buffers
#       wal_buffers
#       max_fsm_relations
#       max_fsm_pages 

echo
echo
echo "#\tChecking the current kernel's shared memory settings..."
echo
echo
# The sysctl calls below echo their own output.

# Changes to files will be made below
pgConfigNote="\n#\tPostgresql Config - Nominatim Setup\n"

# SHMMAX
#
# (BLOCK_SIZE + 208) * ((MAX_MEM_KB * 1024) / PAGE_SIZE) * $SHARED_BUFFER_RATIO) 
SHMMAX=`sysctl $SYSCTL_KERNEL_NAME.shmmax | cut -d'=' -f2`
# Removed the appended zero from the following line (relative to the original) which had the unjustified effect of making it ten times too big
OPTIMAL_SHMMAX=`echo "scale=0; (8192 + 208) * (($MAX_MEM_KB * 1024) / $OS_PAGE_SIZE) * $SHARED_BUFFER_RATIO" | bc -l | cut -d'.' -f1`
echo "OPTIMAL_SHMMAX = $OPTIMAL_SHMMAX"
echo "SHMMAX = $SHMMAX"

if [[ "$SHMMAX" -lt "$OPTIMAL_SHMMAX" ]]; then

    sysctl $SYSCTL_KERNEL_NAME.shmmax=$OPTIMAL_SHMMAX
    echo "${pgConfigNote}$SYSCTL_KERNEL_NAME.shmmax=$OPTIMAL_SHMMAX" >> /etc/sysctl.conf
    # Nullify to avoid repeating the note
    pgConfigNote=
fi

# SHMALL
#
# SHMMAX / PAGE_SIZE 

SHMALL=`sysctl $SYSCTL_KERNEL_NAME.shmall | cut -d'=' -f2`
OPTIMAL_SHMALL=`echo "scale=0; $OPTIMAL_SHMMAX / $OS_PAGE_SIZE" | bc -l | cut -d'.' -f1`
if [[ "$SHMALL" -lt "$OPTIMAL_SHMALL" ]]; then
    sysctl $SYSCTL_KERNEL_NAME.shmall=$OPTIMAL_SHMALL
    echo "${pgConfigNote}$SYSCTL_KERNEL_NAME.shmall=$OPTIMAL_SHMALL" >> /etc/sysctl.conf
    # Nullify to avoid repeating the note
    pgConfigNote=
fi

# MAX_MEM_KB as MB
MAX_MEM_MB=$(echo "scale=0; $MAX_MEM_KB/1024" | bc -l)
SHARED_BUFFERS=$(echo "scale=0; $MAX_MEM_MB * $SHARED_BUFFER_RATIO" | bc -l | cut -d'.' -f1)
# There has been debate on this value on the postgresql mailing lists.
# You might not get any performance gain over 8 GB.  Please test!
if [ $SHARED_BUFFERS -gt 12000 ]; then
       SHARED_BUFFERS=12000MB;
else
       SHARED_BUFFERS="$SHARED_BUFFERS"MB
fi

if [ "$OS_TYPE" = "Linux" -o "$OS_TYPE" = "GNU/Linux" ]; then
       echo "#\tSetting virtual memory sysctls"
       sysctl vm.swappiness=0
       echo "vm.swappiness=0" >>/etc/sysctl.conf
       sysctl vm.overcommit_memory=2
       echo "vm.overcommit_memory=2" >>/etc/sysctl.conf

       # >8GB RAM?  Don't let dirty data build up...this can cause latency issues!
       # These settings taken from "PostgreSQL 9.0 High Performance" by Gregory Smith
       if [ $MAX_MEM_MB -gt 8192 ]; then 
             echo 2 > /proc/sys/vm/dirty_ratio
             echo 1 > /proc/sys/vm/dirty_background_ratio
       else
             echo 10 > /proc/sys/vm/dirty_ratio
             echo 5 > /proc/sys/vm/dirty_background_ratio              
       fi
fi

WAL_BUFFERS="16MB"
EFFECTIVE_CACHE_SIZE=$(echo "scale=0; $MAX_MEM_MB * $EFFECTIVE_CACHE_RATIO" | bc -l | cut -d'.' -f1)MB

# Hard values based on: http://wiki.openstreetmap.org/wiki/Nominatim/Installation#Tuning_PostgreSQL
if [ -n "${override_maintenance_work_mem}" ]; then
    MAINT_WORK_MEM=${override_maintenance_work_mem}
fi
SYNCHRONOUS_COMMIT=off
CHECKPOINT_SEG=100
CHECKPOINT_TIMEOUT=10min
CHECKPOINT_COMPLETION_TARGET=0.9
# At OSMLondon Hack day on Sun 9 Aug 2015 was recommended to use this value instead of 4.0:
RANDOM_PAGE_COST=1.5




#USER=postgres
USER=vagrant
USER_DB_PORT=5432
MAX_USER_CONNECTIONS=100
DATA_NODE_SHARED_BUFFERS="$SHARED_BUFFERS"
DATA_NODE_WORK_MEM="$WORK_MEM"
DATA_NODE_MAINTENANCE_MEM="$MAINT_WORK_MEM"
DATA_NODE_WAL_BUFFERS="$WAL_BUFFERS"
DATA_NODE_CHECKPOINT_SEGMENTS="$CHECKPOINT_SEG"
DATA_NODE_CHECKPOINT_TIMEOUT="$CHECKPOINT_TIMEOUT"
DATA_NODE_CHECKPOINT_COMPLETION_TARGET="$CHECKPOINT_COMPLETION_TARGET"

GMT_MASTER_PORT=6666
COORDINATOR_POOLER_PORT=6668
#DATA_DIR=/var/lib/postgresql


### NOW THE FUN STUFF!!
echo
echo
echo "#\tApplying system configuration settings to the server"
echo "#\tThis system appears to have $MAX_MEM_MB MB maximum memory."
echo
echo



#
mkdir $TARGET_DIR_CONF 2>/dev/null

cat > $TARGET_DIR_CONF/pgxc_ctl.conf <<EOT

# user and path
pgxcOwner=$USER
pgxcUser=\$pgxcOwner
tmpDir=/tmp
localTmpDir=\$tmpDir
configBackup=n
pgxcInstallDir=$TARGET_DIR

#gtm and gtmproxy
gtmName=gtm
gtmMasterServer=localhost
gtmMasterPort=$GMT_MASTER_PORT
gtmMasterDir=$DATA_DIR/data_gtm
gtmSlave=n
gtmProxy=n

#coordinator
coordMasterDirs=($DATA_DIR/data_coord1)
coordNames=(coord1)
coordPorts=($USER_DB_PORT)
poolerPorts=($COORDINATOR_POOLER_PORT)
coordMasterServers=(localhost)
coordMaxWALSenders=(5)
coordSlave=n

#data nodes
datanodeMasterDirs=($DATA_DIR/data_datanode1 $DATA_DIR/data_datanode2)
datanodeNames=(datanode1 datanode2)
datanodePorts=(15432 15433)
datanodeMasterServers=(localhost localhost)
datanodePoolerPorts=(4001 4002)
datanodeMaxWALSenders=(5 5)
datanodeSpecificExtraConfig=(none none)
datanodeSpecificExtraPgHba=(none none)
datanodeSlave=n



# ExtraConfig for a Coordinator
coordExtraConfig=coordExtraConfig
cat > \$coordExtraConfig <<EOF

#================================================
# Added to all the coordinator postgresql.conf

log_destination = 'stderr'
logging_collector = on
log_directory = '/home/vagrant/project/log'
log_filename = 'coordinator.log'

listen_addresses = '*'
max_connections =  $MAX_USER_CONNECTIONS
shared_buffers = $DATA_NODE_SHARED_BUFFERS
work_mem = $DATA_NODE_WORK_MEM
maintenance_work_mem = $DATA_NODE_MAINTENANCE_MEM
wal_buffers = $DATA_NODE_WAL_BUFFERS
checkpoint_segments = $DATA_NODE_CHECKPOINT_SEGMENTS
checkpoint_timeout  = $DATA_NODE_CHECKPOINT_TIMEOUT
checkpoint_completion_target =  $DATA_NODE_CHECKPOINT_COMPLETION_TARGET
random_page_cost = $RANDOM_PAGE_COST
EOF

datanodeExtraConfig=datanodeExtraConfig
cat > \$datanodeExtraConfig <<EOF

#================================================
# Added to all the datanode postgresql.conf

log_destination = 'stderr'
logging_collector = on
log_directory = '/home/vagrant/project/log'
log_filename = 'datanode.log'
max_connections =  $MAX_USER_CONNECTIONS
shared_buffers = $DATA_NODE_SHARED_BUFFERS
work_mem = $DATA_NODE_WORK_MEM
maintenance_work_mem = $DATA_NODE_MAINTENANCE_MEM
wal_buffers = $DATA_NODE_WAL_BUFFERS
checkpoint_segments = $DATA_NODE_CHECKPOINT_SEGMENTS
checkpoint_timeout  = $DATA_NODE_CHECKPOINT_TIMEOUT
checkpoint_completion_target =  $DATA_NODE_CHECKPOINT_COMPLETION_TARGET
random_page_cost = $RANDOM_PAGE_COST


EOF
EOT



