create table members2 (
        id text not null unique,
        email text
) DISTRIBUTE BY HASH(id);


create index on members2(email);

