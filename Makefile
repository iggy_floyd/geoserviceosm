# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# simple makefile to manage this project

description = "An OSM-based GeoLocator as a tool to validate the postal addresses"

all:  doc


doc: README.rst
	-@mkdir doc 2>/dev/null
	-@cd source_doc/; ./create_documentation.sh `ls ../bin/*.py` `ls ../lib/*.py` `ls ../bin/*.sh` `ls ../scripts/*.sh` `ls ../daemons/*.sh`
	-@rst-tool/create_docs.sh README.rst `basename $(PWD)` $(description); mv README.html doc;
	
ifeq (create_config,$(firstword $(MAKECMDGOALS)))
  CREATE_CONFIG_ARGS := $(wordlist 2, $(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(CREATE_CONFIG_ARGS):;@:)
endif

create_config: 
	-@cp template/$(wordlist 1, 1,$(CREATE_CONFIG_ARGS)) configs/config.ini


set_locale:
	-@sudo scripts/set_locale.sh


create_db:
	-@cp templates/create-db.sh.template scripts/create-db.sh
	-@chmod 0755 scripts/create-db.sh
	-@sudo -u postgres scripts/create-db.sh
	-@sudo /etc/init.d/postgresql restart


create_db_sharding:
	-@cp templates/create-db-sharding.sh.template scripts/create-db-sharding.sh
	-@chmod 0755 scripts/create-db-sharding.sh
	-@scripts/create-db-sharding.sh
	-@sudo /etc/init.d/postgresql restart


create_db_postgres_xl:
	-@cp templates/create-db-postgres-xl.sh.template scripts/create-db-postgres-xl.sh
	-@chmod 0755 scripts/create-db-postgres-xl.sh
	-@scripts/remove-db-postgres-xl.sh
	-@scripts/create-db-postgres-xl.sh
	-@scripts/restart-db-postgres-xl.sh
	-@scripts/restart-db-postgres-xl.sh

restart_db_postgres_xl:
	-@sudo /etc/init.d/postgresql stop
	-@scripts/restart-db-postgres-xl.sh
	-@scripts/restart-db-postgres-xl.sh

shutdown_db_postgres_xl:
	-@scripts/shutdown-db-postres-xl.sh

check_ports:
	-@sudo netstat -plnt


clean_db:
	-@sudo -u postgres scripts/clean-db.sh


clean_db_sharding:
	-@sudo -u postgres scripts/clean-db.sh
	-@scripts/clean-db-sharding.sh

clean_db_postgres_xl:
	-@scripts/clean-db-postgres-xl.sh


remove_db_postgres_xl:
	-@scripts/remove-db-postgres-xl.sh



apply_fix:
	-@sudo cp hotfix/app.py.imposm.geocoder /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/app.py
	-@sudo cp hotfix/tables.py /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/tables.py
	-@sudo cp hotfix/__init__.py  /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/__init__.py
	-@sudo cp hotfix/app.py.imposm /usr/local/lib/python2.7/dist-packages/imposm/app.py
	-@sudo cp hotfix/postgis.py.imposm.orig /usr/local/lib/python2.7/dist-packages/imposm/db/postgis.py
	-@sudo cp hotfix/postgis.py.imposm.geocoder /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/db/postgis.py
	-@sudo cp hotfix/osm_geocoder_service.py.orig  bin/osm_geocoder_service.py
	-@chmod 0755 bin/osm_geocoder_service.py
	-@sed  -i -e  's/^geocoding_configs=.*/geocoding_configs=[(\"schema_1\","config_client_1.ini")]/g' lib/settings.py


apply_profiling_fix:
	-@sudo cp hotfix/__init__.py.profiling  /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/__init__.py
	-@sudo cp hotfix/osm_geocoder_service.py.profiling  bin/osm_geocoder_service.py
	-@chmod 0755 bin/osm_geocoder_service.py

apply_sharding_fix:
	-@sudo cp hotfix/__init__.py.sharding  /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/__init__.py
	-@sudo cp hotfix/app.py.imposm.sharding /usr/local/lib/python2.7/dist-packages/imposm/app.py
	-@sudo cp hotfix/postgis.py.imposm.sharding /usr/local/lib/python2.7/dist-packages/imposm/db/postgis.py
	-@sudo cp hotfix/osm_geocoder_service.py.orig  bin/osm_geocoder_service.py
	-@chmod 0755 bin/osm_geocoder_service.py


apply_postgres_xl_fix:
	-@sudo /etc/init.d/postgresql stop
	-@sudo cp hotfix/__init__.py.postgres-xc  /usr/local/lib/python2.7/dist-packages/imposm.geocoder-0.1.4-py2.7.egg/imposm/geocoder/model/__init__.py
	-@sudo cp hotfix/app.py.imposm.postgres-xc /usr/local/lib/python2.7/dist-packages/imposm/app.py
	-@sudo cp hotfix/postgis.py.imposm.postgres-xc /usr/local/lib/python2.7/dist-packages/imposm/db/postgis.py
	-@sudo cp hotfix/osm_geocoder_service.py.orig  bin/osm_geocoder_service.py
	-@chmod 0755 bin/osm_geocoder_service.py
# fix a problem with the memory
	-@sudo sh -c 'echo "vm.swappiness=10" >> /etc/sysctl.conf'
	-@sudo sh -c 'echo "vm.overcommit_memory=0" >> /etc/sysctl.conf'
	-@sudo sysctl -p /etc/sysctl.conf
	-@sed  -i -e  's/^geocoding_configs=.*/geocoding_configs=[(\"schema_1\","config_client_1.ini.new")]/g' lib/settings.py


optimization_postgresql:
	-@sudo scripts/configPostgresql.sh oltp
	-@sudo scripts/configPostgresqlDiskWrites.sh
	-@sudo /etc/init.d/postgresql restart

download_maps:
	-@scripts/download_maps.sh


import_maps:
	-@scripts/import_maps.sh 


check_db:
	-@sudo -u postgres scripts/check-db.sh 


osm_update_cronjob_add:
	-@scripts/create-cronjob-update.sh

backup_db:
	-@scripts/backup-db.sh

restore_db:
	-@scripts/restore-db.sh

backup_db_postgres_xl:
	-@scripts/backup-db-postgres-xl.sh

restore_db_postgres_xl:
	-@scripts/restore-db-postgres-xl.sh

start_daemons:
	-@cd daemons; ./setup.daemons.sh; ./process_all_daemons.sh start

status_daemons:
	-@cd daemons; ./setup.daemons.sh; ./process_all_daemons.sh status

stop_daemons:
	-@cd daemons; ./setup.daemons.sh; ./process_all_daemons.sh stop


# to clean all temporary stuff
clean:  
	-@rm -r doc 
	-@rm README

.PHONY: clean all doc create_config set_locale create_db clean_db apply_fix download_maps import_maps check_db osm_update_cronjob_add start_daemons status_daemons stop_daemons optimization_postgresql create_db_sharding clean_db_sharding create_db_postgres_xl clean_db_postgres_xl remove_db_postgres_xl apply_profiling_fix apply_sharding_fix apply_postgres_xl_fix check_ports restore_db_postgres_xl backup_db restore_db backup_db_postgres_xl
