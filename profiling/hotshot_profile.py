#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Hotshot Profiler
# ==============================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   hotshot_profile.py
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#


'''
   The Hotshot Profiler

'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"

# Requirements
# ------------
#
# ::
import hotshot
import sys

# Hotshot decorator
# --------------------------
# This decorator can be used for profiling functions
# ::


hotshotProfilers = {}
def hotshotit(func):
    def wrapper(*args, **kw):
        import hotshot
        global hotshotProfilers
        prof_name = func.func_name+".prof"
        profiler = hotshotProfilers.get(prof_name)
        if profiler is None:
            profiler = hotshot.Profile(prof_name)
            hotshotProfilers[prof_name] = profiler
        return profiler.runcall(func, *args, **kw)
    return wrapper


# The test suite of the hotshot_profile.py
# -------------------------------------
#
#  .. code-block:: bash
#
#                  vagrant@FPS-Fraud-Classifier2:~/project/lib> python hotshot_profile.py 200000
#
# ::


if __name__ == "__main__":

   @hotshotit
   def  my_test_function():
     '''my test function'''
     _sum=0
     for i in range(int(sys.argv[1])):
      _sum+=i
     print _sum

   my_test_function()
