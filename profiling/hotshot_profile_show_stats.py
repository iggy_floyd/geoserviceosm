#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Hotshot Profiler Statistics
# ==============================
#
# :Date: Apr 23, 2015, 12:59:09 PM
# :File:   hotshot_profile_show_stats.py
# :Copyright: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
#


'''
   The Hotshot Profiler Statistics
   usage: python hotshot_profile_show_stats.py foo.prof 20


'''

__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$Apr 23, 2015 12:59:09 PM$"
__docformat__ = 'restructuredtext'
__version__ = "0.0.1"


# Requirements
# ------------
#
# ::
import hotshot,hotshot.stats
import sys


# The main entrance of hotshot_profile.py
# ----------------------------------------------
#
#  .. code-block:: bash
#
#                  vagrant@FPS-Fraud-Classifier2:~/project/lib> ./hotshot_profile_show_stats.py foo.prof 20
#
# ::


if __name__ == "__main__":

   stats = hotshot.stats.load(sys.argv[1])
   stats.strip_dirs()
   stats.sort_stats('time', 'calls')
   stats.print_stats(int(sys.argv[2]))

